import numpy as np
import ROOT 
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel 
import sys
sys.path.append('inc/')
from FallExpKernel import Gibbs
from LinearErrorKernel import LinearNoiseKernel
from utils import generateBackgroundWorkspace, generateSignalWorkspace, getInputXAxis, makeBinning, makeGPtemplate
from myFunctions import define_background_functions
import multiprocessing 


########### User Defined Options ##########

# These are all pulled from the inc/inputs.py file 

import inputs as userin 

###########################################

### Perform the Scan of GPR Hyper Parameters for a Signal Width ### 

def performScan( 	inHisto , 
					catName , 
					length_scales , 
					length_scale_slopes ,
					whichKernel , 
					whichGPMean , 
					basisBkgWS , 
					basisSigWS , 
					sigWidth , 
					sigPos , 
					sigYield , 
					nBinsToPad ):
			
	myBins = makeBinning(inHisto)
	nBins = inHisto.GetNbinsX()
	
	xMin = inHisto.GetBinLowEdge(1)
	xMax = inHisto.GetBinLowEdge( inHisto.GetNbinsX()+1 )
	
	Nevts = inHisto.GetSumOfWeights()			
	
	### Create the combined "signal"+bkg toy for this set of parameters 
	
	sigPDF_name = "sig"
	sigPDF_widthName = "sigWidth"
	sigPDF_posName = "RooM0"
	sigPDF_obsName = "rooMyy"
		
	# If signal workspace has been pulled from an input file, need to get the names of the parameters 
	if( not hasattr(userin, 'signalPDFs') ): userin.signalPDFs = "" 
	if( userin.signalPDFs ):
	
		sigPDF_obsName = ( signalDataSets[iCat].get() ).first().GetName()			
		signalDataSets[iCat].changeObservableName( sigPDF_obsName , "rooMyy" )
	
		signalPDF = signalPDFs[iCat]
		sigPDF_name = signalPDF.GetName() 
		pars = signalPDF.getParameters( userin.signalDataSets[iCat] )	
		variter = pars.createIterator()
		whichvar = variter.Next()
		
		while whichvar:
			if( userin.signalFile_sigWidthNametag in whichvar.GetName() ): 
				sigPDF_widthName = whichvar.GetName()
			if( userin.signalFile_sigMeanNametag in whichvar.GetName() ): 
				sigPDF_posName = whichvar.GetName() 
			if( whichvar.GetName() == sigPDF_obsName ): 
				whichvar.SetName("rooMyy")
				whichvar.setRange(xMin,xMax)
			whichvar = variter.Next()
		
	basisSigWS.var(sigPDF_widthName).setVal( sigWidth )
	basisSigWS.var(sigPDF_posName).setVal( sigPos )
	basisBkgWS.var("rooMyy").setRange(xMin,xMax)

	h_basisBkgPDF = basisBkgWS.pdf("bkg").createHistogram("h_basisBkgPDF",basisBkgWS.var("rooMyy"),ROOT.RooFit.Binning(myBins))
	h_basisBkgPDF.Scale( Nevts / h_basisBkgPDF.GetSumOfWeights() )
	
	h_basisSigPDF = basisSigWS.pdf(sigPDF_name).createHistogram("h_basisSigPDF",basisSigWS.var("rooMyy"),ROOT.RooFit.Binning(myBins))
	h_basisSigPDF.Scale( sigYield / h_basisSigPDF.GetSumOfWeights() )


	myToy = ROOT.TH1D("myToy","myToy",nBins,myBins.array())
	myToy.Sumw2()
	myToy.Add(h_basisBkgPDF)
	myToy.Add(h_basisSigPDF)
	#for bin in range(myToy.GetNbinsX()): myToy.SetBinError(bin+1, inHisto.GetBinError(bin+1) )	
	for bin in range(myToy.GetNbinsX()): myToy.SetBinError(bin+1, 0.001*inHisto.GetBinContent(bin+1) )	
	
	
	### Construct a signal+bkg fit to the smoothed template to evaluate new fitted signal 
	testSmoothingWS = ROOT.RooWorkspace("testSmoothingWS")
	
	getattr(testSmoothingWS,'import')( basisSigWS.pdf(sigPDF_name) ) 
	getattr(testSmoothingWS,'import')( basisBkgWS.pdf("bkg") ) 
	
	testSmoothingWS.factory('Ns[{0}, 0, {1}]'.format(sigYield, 2.0*sigYield))
	testSmoothingWS.factory('Nb[{0}, 0, {1}]'.format(Nevts, 1.1*Nevts))
	testSmoothingWS.factory("SUM:sbModel(Ns*"+sigPDF_name+",Nb*bkg)")
	
	
	allPdfVars = testSmoothingWS.allVars() 
	nonConstVars = ["rooMyy","Ns","Nb"] 
	variter = allPdfVars.createIterator()
	whichvar = variter.Next()
	while whichvar:
		if( whichvar.GetName() not in nonConstVars ): 
			testSmoothingWS.var( whichvar.GetName() ).setConstant(ROOT.kTRUE) 
		whichvar = variter.Next() 
	
	### Perform the many GP fits across the hyperparameter ranges
	
	gpTemplates = [ [ [] for j in range(len(length_scale_slopes)) ] for i in range(len(length_scales)) ]
	diffHistos = [ [ [] for j in range(len(length_scale_slopes)) ] for i in range(len(length_scales)) ]	
	fittedSignals = np.zeros( (len(length_scales),len(length_scale_slopes)) , 'd' ) 
	
	checkins = []
	
	### Perform Scan using Multiprocessing ###
# 	if( useMultiprocessing ):
# 
# 		# Unpack inputs 
# 		ins = []
# 
# 		for i,ls in enumerate(length_scales):
# 			for j,ls_b in enumerate(length_scale_slopes):	
# 				ins.append( (myToy,userin.whichKernel,ls,[ls,ls],ls_b,[ls_b,ls_b],whichGPMean,nBinsToPad,1,1,False,0.0) )
# 				checkins.append( [ls,ls_b] )
# 	
# 		pool = multiprocessing.Pool(multiprocessing.cpu_count())
# 		results = pool.starmap(makeGPtemplate,ins)
# 		pool.close()
# 		
# 		itr1d = 0 
# 		for i in range(len(length_scales)):
# 			for j in range(len(length_scale_slopes)):	
# 				gpTemplates[i][j] = results[itr1d][0]
# 				
# 				print( "debug = ", "in=",checkins[itr1d],"  comp: ",length_scales[i],",",length_scale_slopes[j])
# 				
# 				itr1d += 1 
	
	### Perform Scan in Series ###	
	for i,ls in enumerate(length_scales):
		for j,ls_b in enumerate(length_scale_slopes):	
		# Perform the GP fit 
		
			print("Evaluating hyperparameters: length scale = ",ls,", length scale slope = ", ls_b)

			gpTemplates[i][j] = makeGPtemplate( myToy , 
												userin.whichKernel , 
												ls , [ ls , ls ] , 
												ls_b , [ ls_b , ls_b ] , 
												whichGPMean , 
												nBinsToPad , 
												1 , 1 , 
												False , 
												0.0 )[0]
			
			
			dummyGPRooHist = ROOT.RooDataHist("dummyGPRooHist", "dummyGPRooHist", ROOT.RooArgList(testSmoothingWS.obj("rooMyy")), gpTemplates[i][j])
			testSmoothingWS.pdf("sbModel").fitTo( dummyGPRooHist , ROOT.RooFit.Range(xMin, xMax) , ROOT.RooFit.SumW2Error(ROOT.kTRUE) , ROOT.RooFit.PrintLevel(-1) )
			fittedSignals[i,j] = testSmoothingWS.var("Ns").getValV() 
				

	### Evaluate the S+B Fit Model to Extract Retained Signal ###
	
	for i,ls in enumerate(length_scales):
		for j,ls_b in enumerate(length_scale_slopes):	

			hName = "h_gprPred_ls_"+str(ls)+"_lsb_"+str(ls_b)+"_sw_"+"{:4.2f}".format(sigWidth) 		
			gpTemplates[i][j].SetName(hName) 
			
			hName = "h_diff_ls_"+str(ls)+"_lsb_"+str(ls_b)+"_sw_"+"{:4.2f}".format(sigWidth)
			diffHistos[i][j] = ROOT.TH1D(hName,hName,nBins,myBins.array())
			diffHistos[i][j].Sumw2()
			diffHistos[i][j].Add( gpTemplates[i][j] , myToy , 1.0 , -1.0 )
			
			dummyGPRooHist = ROOT.RooDataHist("dummyGPRooHist", "dummyGPRooHist", ROOT.RooArgList(testSmoothingWS.obj("rooMyy")), gpTemplates[i][j])
			testSmoothingWS.pdf("sbModel").fitTo( dummyGPRooHist , ROOT.RooFit.Range(xMin, xMax) , ROOT.RooFit.SumW2Error(ROOT.kTRUE) , ROOT.RooFit.PrintLevel(-1) )
			fittedSignals[i,j] = testSmoothingWS.var("Ns").getValV() 
			
			### Some plots for debugging 			
# 			testSmoothingWS.pdf("bkg").fitTo( dummyGPRooHist , ROOT.RooFit.Range(xMin, xMax) , ROOT.RooFit.SumW2Error(ROOT.kTRUE)) 
# 			testFrame = testSmoothingWS.obj('rooMyy').frame() 
# 			dummyToyRooHist = ROOT.RooDataHist("myRooToy", "myRooToy", ROOT.RooArgList(testSmoothingWS.obj("rooMyy")), myToy)
# 			dummyToyRooHist.plotOn( testFrame , ROOT.RooFit.MarkerColor(1) )
# 			dummyGPRooHist.plotOn( testFrame , ROOT.RooFit.MarkerColor(2) )
# 			testSmoothingWS.pdf('sbModel').plotOn( testFrame , ROOT.RooFit.LineColor(415) , ROOT.RooFit.LineWidth(6) , ROOT.RooFit.Name('SBFit') , ROOT.RooFit.Normalization(gpTemplates[i][j].GetSumOfWeights(), ROOT.RooAbsReal.NumEvent ) ) 		
# 			testSmoothingWS.pdf('bkg').plotOn( testFrame , ROOT.RooFit.LineColor(415) , ROOT.RooFit.LineWidth(6) , ROOT.RooFit.Name('bkgOnlyFit') , ROOT.RooFit.Normalization(gpTemplates[i][j].GetSumOfWeights(), ROOT.RooAbsReal.NumEvent ) ) 		
# 			testFrame.Draw()
# 			import pdb; pdb.set_trace() 

			dummyGPRooHist.Delete() 
			
			
	### Plot 
	fitCan = ROOT.TCanvas("fitCan","fitCan",800,800)
	upperPad = ROOT.TPad("upperPad", "upperPad", 0.0, 0.3, 1.0, 1.0)
	lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.0, 0.0, 1.0, 0.3)
	upperPad.Draw() 
	upperPad.SetBottomMargin(0.02)
	lowerPad.Draw() 
	lowerPad.SetTopMargin(0.02) ;
	lowerPad.SetBottomMargin(0.3) ;
	fitLeg = ROOT.TLegend(0.65,0.30,0.93,0.93)
	fitLeg.SetFillColor(-1)
	fitLeg.SetBorderSize(0)
	fitLeg.SetNColumns(3);
	ROOT.gStyle.SetPalette(60)
	
	upperPad.cd() 
	
	myToy.GetYaxis().SetTitle("Events / Bin")
	myToy.SetTitle(" ")
	myToy.SetMarkerSize(1)
	myToy.SetMarkerColor(1)
	#myToy.SetLineWidth(0)
	myToy.SetStats(0)
	myToy.GetXaxis().SetTitleSize(0)
	myToy.GetXaxis().SetLabelSize(0)
	myToy.GetYaxis().SetTitle("Events / Bin")
	myToy.SetTitle(" ") 
	myToy.Draw("ep")
	
	for i in range(len(length_scales)):
		for j in range(len(length_scale_slopes)): 
			gpTemplates[i][j].SetMarkerSize(0)
			gpTemplates[i][j].SetLineWidth(2)
			gpTemplates[i][j].Draw("ehist same plc")
			fitLeg.AddEntry( gpTemplates[i][j] , "#lambda="+str(length_scales[i])+", b_{#lambda}="+str(length_scale_slopes[j]) )
	
	ROOT.ATLAS_LABEL(0.25,0.85)
	ROOT.myText(0.35,0.85,1,"Internal")
	ROOT.myText(0.25,0.81,1,"GPR Fits",0.04)
	ROOT.myText(0.25,0.77,1,catName,0.04)
	ROOT.myText(0.25,0.73,1,"Injected #sigma = "+"{:4.2f}".format(sigWidth),0.04)
	fitLeg.Draw()
	
	lowerPad.cd() 
	refLine = ROOT.TH1I("refLine","refLine",1,xMin,xMax)
	refLine.SetBinContent(1,0) 
	refLine.GetXaxis().SetTitle("M_{#gamma#gamma} [GeV]")
	refLine.GetXaxis().SetTitleSize(0.13)
	refLine.GetXaxis().SetTitleOffset(1.0)
	refLine.GetXaxis().SetLabelSize(0.12)
	refLine.GetYaxis().SetTitle("Fit - True")
	refLine.GetYaxis().SetTitleSize(0.125)
	refLine.GetYaxis().SetTitleOffset(0.55)
	refLine.GetYaxis().SetLabelSize(0.12)
	refLine.SetLineColor(1)
	refLine.SetLineWidth(1)
	refLine.SetLineStyle(2)
	refLine.SetMinimum(-0.24)
	refLine.SetMaximum(0.24)
	refLine.Draw()
	
	for i in range(len(length_scales)):
		for j in range(len(length_scale_slopes)): 
			diffHistos[i][j].Divide(myToy)
			diffHistos[i][j].SetMarkerSize(0)
			diffHistos[i][j].SetLineWidth(2)
			diffHistos[i][j].Draw("ehist same plc")		
	
	inHistName = inHisto.GetName() 
	fitCan.Print("GPRHyperParOpt_Fits_"+inHistName+"_sigma_"+"{:4.2f}".format(sigWidth)+".eps") 
	
	return fittedSignals 


###########################################
########### The Important Stuff ###########

def main(): 

	### Extract input templates and information 

	nCategories = len(userin.templateHistoNames) 
	
	signalWidths = [] 
	signalPositions = [] 
	signalYields = [] 
	signalPDFs = [] 
	signalDataSets = []
	signalFractions = [] 

	if( not 'categoryNames' in globals() ): categoryNames = ""		
	if ( len(categoryNames) != nCategories ): 
		print( 	"WARNING - The number of category names passed is not the same as the number of background templates passed! Will ignore the given category names." )
		categoryNames = userin.templateHistoNames 

	### Extract the information from the signal parameterization file 
	if( not hasattr(userin, 'inSignalFileName') ): userin.inSignalFileName = "" 
	if( not not userin.inSignalFileName ): # if a file name is provided, extract info from this 
		userin.inSignalFile = ROOT.TFile.Open(userin.inSignalFileName,"READ") 
		signalFileWS = userin.inSignalFile.Get(userin.inSignalWorkspaceName)
		
		for iCat in range(nCategories): 
			signalPDFs.append( signalFileWS.pdf( userin.signalFile_sigPDFNames[iCat] ) )
			signalDataSets.append( signalFileWS.data(userin.signalFile_sigDataNames[iCat]) )
					
			sigPDF_name = userin.signalFile_sigPDFNames[iCat]
			pars = signalPDFs[iCat].getParameters( signalDataSets[iCat] )	
			variter = pars.createIterator()
			whichvar = variter.Next()
		
			sigPDF_widthName = ""
			sigPDF_posName = ""
		
			while whichvar:
				print(whichvar.GetName())
				if( userin.signalFile_sigWidthNametag in whichvar.GetName() ): 
					sigPDF_widthName = whichvar.GetName()
				elif( userin.signalFile_sigMeanNametag in whichvar.GetName() ): 
					sigPDF_posName = whichvar.GetName()
				whichvar = variter.Next()
								
			if( ( not sigPDF_posName ) or ( not sigPDF_widthName ) ): 
				print("ERROR! The naming scheme of the width and/or mean variables from the input signal file appears to differ from the names provided. Check these in inc/inputs.py (i.e. signalFile_sigWidthNametag, etc.).")
				return 1
				
			signalWidths.append( (signalFileWS.obj(sigPDF_widthName)).getVal() )
			signalPositions.append( (signalFileWS.obj(sigPDF_posName)).getVal() )
			signalYields.append( signalDataSets[iCat].sumEntries() ) 
	
		if( len(signalWidths) != nCategories or len(signalPositions) != nCategories or len(signalYields) != nCategories ):
			print( "ERROR - The provided signal parameterization file doesn't contain the same number of signal shapes as the number of categories")
			print( "        Try checking the signalFile_sigXXParameters lists to ensure there are no missing entries" )

	### Extract the information from the values provided in the input file (no input signal WS)
	else: 
		if( not hasattr(userin, 'signalYields') ): signalYields = [] 
		else: signalYields = userin.signalYields
		if( len(signalYields) < 1 ): 
			useSignalFracs = True 
			if ( len(userin.signalFractions) == 1 ): signalFractions = [ userin.signalFractions[0] for i in range(nCategories) ] 
			signalYields = [ -1.0 for i in range(nCategories) ]
		elif ( len(signalYields) == 1 ): signalYields = [ userin.signalYields[0] for i in range(nCategories) ] 
		
	
		signalWidths = userin.signalWidths
		signalPositions = userin.signalPositions

		# If only one parameter passed for injected "signal", use for all categories 
		if ( len(userin.signalWidths) == 1 ): signalWidths = [ userin.signalWidths[0] for i in range(nCategories) ] 
		if ( len(userin.signalPositions) == 1 ): signalPositions = [ userin.signalPositions[0] for i in range(nCategories) ] 

		# If multiple parameters passed, need to make sure the # of parameters is the same as the # of categories. Otherwise, break!
		if ( len(signalWidths) != nCategories ): 
			print( 	"WARNING - The number of signal widths passed is not the same as the number of background templates passed! Breaking." )
			return 1 
		elif ( len(signalPositions) != nCategories ): 
			print( 	"WARNING - The number of signal x-positions passed is not the same as the number of background templates passed! Breaking." )		
			return 1 
		elif ( len(signalYields) != nCategories ): 
			print( 	"WARNING - The number of signal yields passed is not the same as the number of background templates passed! Breaking." )
			return 1 				


	outputTrees = [] 
	outFile = ROOT.TFile(userin.hyperParFileName,"RECREATE") 
	inFile = ROOT.TFile.Open(userin.inFileName,"READ")
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasStyle.C") 
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasUtils.C") 
	ROOT.SetAtlasStyle()

	# Branches for the output tree containing the "good" area
	b_length_scale_min = []
	b_length_scale_max = []
	b_length_scale_slope_min = []
	b_length_scale_slope_max = []

	# Branches for the individual scan results 
	b_length_scale_testVal = [] 
	b_length_scale_slope_testVal = [] 
	b_signal_width = [] 
	b_fitted_signal = [] 
	b_injected_signal_yield = [] 
	b_fractional_feature_smoothing = []

	for iCat in range(nCategories): 

		### Setup all inputs before applying GPR 
		
		inHistName = userin.templateHistoNames[ iCat ]
		if( len(categoryNames) ): catName = categoryNames[ iCat ] 
		else: catName = inHistName
	
		print( "Optimizing GP Hyperparameters of Category: ", catName )
	
		### Check if using RBF kernel (if so, no need to scan length_scale_slopes)
	
		if( userin.whichKernel == "RBF" ): length_scale_slopes = [ 0 ] 

		# Get the input template histogram 

		inFile = ROOT.TFile.Open(userin.inFileName,"READ")
		inHisto = inFile.Get(inHistName)
	
		if( not inHisto ): 
			print( "ERROR - Template '"+inHistName+"' not found!! Check template name and file contents." )
			continue 
	
		if( userin.nRebin != 1 ): inHisto.Rebin( userin.nRebin )
		
		myBins = makeBinning(inHisto) # the bin edges (for defining histograms)
		xMin = myBins.lowBound() 
		xMax = myBins.highBound()
		Nevts = inHisto.GetSumOfWeights() 

		### Determine the Optimal Analytical Function to Use 
		# Want to model the shape of the template, but without any statistical fluctuations 
	
		myBkgFunctions = define_background_functions() 
		nDOF = np.array([],'i')
		allChiSqrd = np.array([],'d')
	
		for i,funcDef in enumerate(myBkgFunctions): 
			testFunction = funcDef[0]
			testBkgWS = generateBackgroundWorkspace( "test"+testFunction+"WS" , testFunction , [ xMin , xMax ] ) 
			testerRooHist = ROOT.RooDataHist("testerRooHist", "testerRooHist", ROOT.RooArgList( testBkgWS.obj("rooMyy") ), inHisto)
			testBkgWS.pdf("bkg").fitTo( testerRooHist , ROOT.RooFit.SumW2Error(ROOT.kTRUE) , ROOT.RooFit.PrintLevel(-1) ) 
			testFrame = testBkgWS.obj('rooMyy').frame() 
			testerRooHist.plotOn( testFrame , ROOT.RooFit.MarkerColor(1) )
			testBkgWS.pdf('bkg').plotOn( testFrame , ROOT.RooFit.LineColor(415) , ROOT.RooFit.LineWidth(6) , ROOT.RooFit.Name('bkgOnlyFit')) 		
			nDOF = np.append( nDOF , testBkgWS.pdf('bkg').getVariables().getSize() )
			allChiSqrd = np.append( allChiSqrd , testFrame.chiSquare( int(nDOF[i]) ) )
		
		bestFunctionNum = np.where( np.abs(1.0 - allChiSqrd ) == np.abs(1.0 - allChiSqrd ).min() )[0][0]
		whichFunction = myBkgFunctions[bestFunctionNum][0]

		### Create workspaces containing the specified signal and background shapes 

		# Extract the signal shape for this category from the input signal file 
	
		if( 'signalFileWS' in globals() ): basisSigWS = signalFileWS
		else: basisSigWS = generateSignalWorkspace( "signalWS" , userin.whichSigShape , [ xMin , xMax ] )
	
		basisBkgWS = generateBackgroundWorkspace( "backgroundWS" , whichFunction , [ xMin , xMax ] )
		inRooHisto = ROOT.RooDataHist("inRooHisto", "inRooHisto", ROOT.RooArgList(basisBkgWS.obj("rooMyy")), inHisto) 
		basisBkgWS.obj("bkg").fitTo( inRooHisto , ROOT.RooFit.PrintLevel(-1) , ROOT.RooFit.SumW2Error(ROOT.kTRUE) ) 	
				
		### Now Run! 
	
		if( not 'nBinsToPad' in globals() ): nBinsToPad = 0 
	
		sigPos = signalPositions[iCat]
		sigBinWidth = inHisto.GetBinWidth( inHisto.FindBin(sigPos) )
	
		if( signalYields[iCat] < 0 ): signalYields[iCat] =  Nevts * signalFractions[iCat]
		sigYield = signalYields[iCat]
		
		length_scales = userin.length_scales 
		length_scale_slopes = userin.length_scale_slopes

		fittedSignals = [] 
		fracFeatureSmoothing = []
		for sigWidth in [ 0.5*sigBinWidth , signalWidths[iCat] ]:
			if( sigWidth==0.5*sigBinWidth and ( userin.narrowFeatureTolerance == 0 ) ): 
				fittedSignals.append( np.zeros( (len(length_scales),len(length_scale_slopes)) , 'd' ) )
				fracFeatureSmoothing.append( np.zeros( (len(length_scales),len(length_scale_slopes)) , 'd' ) )
			else: 			
				fittedSignals.append( performScan( 	inHisto , 
													catName , 
													length_scales , 
													length_scale_slopes , 
													userin.whichKernel , 
													userin.whichGPMean , 
													basisBkgWS , 
													basisSigWS , 
													sigWidth , 
													sigPos , 
													sigYield , 
													nBinsToPad ) )
				fracFeatureSmoothing.append( ( sigYield - fittedSignals[-1] ) / ( sigYield ) )
	
		### Plot the best hyperparameters 

		# Check if using RBF kernel (if so, don't scan over length_scale_slopes)
		if( len(length_scale_slopes)==1 ): 
			length_scale_slopes = [ length_scale_slopes[0] , length_scale_slopes[0] + 0.001 ] # this is just a dummy bin for plotting
			for i in range(len(fracFeatureSmoothing)):
				fracFeatureSmoothing[i] = np.hstack((fracFeatureSmoothing[i],fracFeatureSmoothing[i]))
	
		xBins = np.array(length_scales,'d')
		yBins = np.array(length_scale_slopes,'d') 
	
		hyperParCan = ROOT.TCanvas("hyperParCan","hyperParCan",800,600)
		outHistoName = "goodHyperPars_"+inHistName
		goodValHisto = ROOT.TH2I(outHistoName,outHistoName,len(xBins)-1,xBins,len(yBins)-1,yBins)
	
		# Determine the best minimum length scale (GPR fit smoothes out at least X% of narrow peak, with X defined in inputs) 
		for i in range(len(length_scales)): 
			for j in range(len(length_scale_slopes)): 
				if ( fracFeatureSmoothing[0][i,j] >= ( userin.narrowFeatureTolerance ) ): 
					goodValHisto.Fill( length_scales[i] , length_scale_slopes[j] , 1 ) 

		# Determine the best maximum length scale (GPR fit retains at least X% of wider peak, with X defined in inputs) 
		if type(userin.largeFeatureTolerance) is str: 
			statErrorFrac = float(userin.largeFeatureTolerance.split("*")[0])
			catSigYieldStatError = np.sqrt( signalYields[iCat] )
			yieldTolerance = statErrorFrac * catSigYieldStatError 
			userin.largeFeatureTolerance = yieldTolerance / signalYields[iCat]
	
		for i in range(len(length_scales)-1): 
			for j in range(len(length_scale_slopes)-1): 
				if ( fracFeatureSmoothing[1][i,j] <= userin.largeFeatureTolerance and 
					fracFeatureSmoothing[1][i+1,j] <= userin.largeFeatureTolerance and 
					fracFeatureSmoothing[1][i,j+1] <= userin.largeFeatureTolerance  ): 
						goodValHisto.Fill( length_scales[i] , length_scale_slopes[j] , 1 ) 
	
		# Fill the output histogram with a value of 1 if the hyper params pass both conditions above 
		for xbin in range(goodValHisto.GetNbinsX()): 
			for ybin in range(goodValHisto.GetNbinsY()): 
				bin = goodValHisto.GetBin(xbin+1,ybin+1)
				if( goodValHisto.GetBinContent(bin) == 2 ): goodValHisto.SetBinContent(bin,1)
				else: goodValHisto.SetBinContent(bin,0)	
	
		# Select a rectangular area out of the optimal hyper params such that the length scale bounds are the widest possible, and favoring larger length scales 	
		holder_ls_bounds = [] 
		for ybin in range(goodValHisto.GetNbinsY()): 
			ls_min = 999
			ls_max = -999 
			for xbin in range(goodValHisto.GetNbinsX()): 
				bin = goodValHisto.GetBin(xbin+1,ybin+1)
				if( goodValHisto.GetBinContent(bin) == 1 ): 
					if( ls_min > goodValHisto.GetXaxis().GetBinLowEdge(xbin+1) ): ls_min =goodValHisto.GetXaxis().GetBinLowEdge(xbin+1)
					if( ls_max < goodValHisto.GetXaxis().GetBinLowEdge(xbin+2) ): ls_max =goodValHisto.GetXaxis().GetBinLowEdge(xbin+2)
			holder_ls_bounds.append([ls_min,ls_max])
	
		bls_bounds = [-1,-1] 
		ls_bounds = [-1,-1]
		lswidth = 0 
		for ybin in range(goodValHisto.GetNbinsY()): 
			if( lswidth < holder_ls_bounds[ybin][1]-holder_ls_bounds[ybin][0] ): 
				lswidth = holder_ls_bounds[ybin][1]-holder_ls_bounds[ybin][0] 
				bls_bounds = [ yBins[ybin] , yBins[ybin+1] ]
				ls_bounds = holder_ls_bounds[ybin]
			elif( lswidth == holder_ls_bounds[ybin][1]-holder_ls_bounds[ybin][0] and holder_ls_bounds[ybin]==holder_ls_bounds[ybin-1] ): 
				bls_bounds[1] = yBins[ybin+1] 
				ls_bounds = holder_ls_bounds[ybin]
			elif( lswidth == holder_ls_bounds[ybin][1]-holder_ls_bounds[ybin][0] and holder_ls_bounds[ybin][1]>holder_ls_bounds[ybin-1][1] ): 
				bls_bounds = [ yBins[ybin] , yBins[ybin+1] ]
				ls_bounds = holder_ls_bounds[ybin]

		# Fill the output histogram with a value of 2 if the hyper params are in the optimal rectangle 
		for xbin in range(goodValHisto.GetNbinsX()): 
			for ybin in range(goodValHisto.GetNbinsY()): 
				bin = goodValHisto.GetBin(xbin+1,ybin+1)
				if( goodValHisto.GetXaxis().GetBinCenter(xbin+1) > ls_bounds[0] and 
					goodValHisto.GetXaxis().GetBinCenter(xbin+1) < ls_bounds[1] and 
					goodValHisto.GetYaxis().GetBinCenter(ybin+1) > bls_bounds[0] and 
					goodValHisto.GetYaxis().GetBinCenter(ybin+1) < bls_bounds[1] ):
					goodValHisto.SetBinContent(bin,2)
	
		# Stylize the output plot 
		goodValHisto.SetStats(0)
		goodValHisto.GetXaxis().SetTitle("Gibbs Length Scale [GeV]")
		goodValHisto.GetYaxis().SetTitle("Gibbs Length Scale Slope")
		goodValHisto.SetTitle(" ") 
		ROOT.gStyle.SetPalette(70)
		ROOT.TColor.InvertPalette()
		goodValHisto.Draw("col")
		ROOT.ATLAS_LABEL(0.20,0.85)
		ROOT.myText(0.32,0.85,1,"Internal")
		ROOT.myText(0.20,0.81,1,"GPR Hyper Parameters",0.04)
		ROOT.myText(0.20,0.77,1,catName,0.04)
		hyperParCan.Update() 
		hyperParCan.Print("GoodHyperPars_"+inHistName+".eps")
		
		goodValHisto.SetName(outHistoName) 
		outFile.cd() 
		goodValHisto.Write()
	
		# Make a ROOT tree with the selected hyper-parameter rectangular area 
		outputTrees.append( ROOT.TTree("HyperParArea_"+inHistName,"HyperParArea_"+inHistName) )
	
		b_length_scale_min.append( np.zeros(1, dtype=float) )
		b_length_scale_max.append( np.zeros(1, dtype=float) )
		b_length_scale_slope_min.append( np.zeros(1, dtype=float) )
		b_length_scale_slope_max.append( np.zeros(1, dtype=float) )
	
		outputTrees[-1].Branch("length_scale_min",b_length_scale_min[-1],"length_scale_min/D")
		outputTrees[-1].Branch("length_scale_max",b_length_scale_max[-1],"length_scale_max/D")
		outputTrees[-1].Branch("length_scale_slope_min",b_length_scale_slope_min[-1],"length_scale_slope_min/D")
		outputTrees[-1].Branch("length_scale_slope_max",b_length_scale_slope_max[-1],"length_scale_slope_max/D")
	
		b_length_scale_min[-1][0] = ls_bounds[0] 
		b_length_scale_max[-1][0] = ls_bounds[1] 
		b_length_scale_slope_min[-1][0] = bls_bounds[0] 
		b_length_scale_slope_max[-1][0] = bls_bounds[1] 
	
		outputTrees[-1].Fill() 
	
		# Make a ROOT tree with the full scan results 
	
		outputTrees.append( ROOT.TTree("HyperParDetailedResults_"+inHistName,"HyperParDetailedResults_"+inHistName) )
	
		b_length_scale_testVal.append( np.zeros(1, dtype=float) )
		b_length_scale_slope_testVal.append( np.zeros(1, dtype=float) )
		b_signal_width.append( np.zeros(1, dtype=float) )
		b_fitted_signal.append( np.zeros(1, dtype=float) )
		b_injected_signal_yield.append( np.zeros(1, dtype=float) )
		b_fractional_feature_smoothing.append( np.zeros(1, dtype=float) )

		outputTrees[-1].Branch("length_scale_testVal",b_length_scale_testVal[-1],"length_scale_testVal/D")
		outputTrees[-1].Branch("length_scale_slope_testVal",b_length_scale_slope_testVal[-1],"length_scale_slope_testVal/D")
		outputTrees[-1].Branch("signal_width",b_signal_width[-1],"signal_width/D")
		outputTrees[-1].Branch("fitted_signal",b_fitted_signal[-1],"fitted_signal/D")
		outputTrees[-1].Branch("injected_signal_yield",b_injected_signal_yield[-1],"injected_signal_yield/D")
		outputTrees[-1].Branch("fractional_feature_smoothing",b_fractional_feature_smoothing[-1],"fractional_feature_smoothing/D")
	
		for isw,sigWidth in enumerate([ 0.5*sigBinWidth , signalWidths[iCat] ]): 
		
			b_signal_width[-1][0] = sigWidth
			b_injected_signal_yield[-1][0] = sigYield 
		
			for i,ls in enumerate(length_scales):
				for j,ls_b in enumerate(length_scale_slopes):
	
					b_length_scale_testVal[-1][0] = ls
					b_length_scale_slope_testVal[-1][0] = ls_b 
				
					b_fitted_signal[-1][0] = fittedSignals[isw][i][j] 
					b_fractional_feature_smoothing[-1][0] = fracFeatureSmoothing[isw][i][j] 
				
					outputTrees[-1].Fill()
	
	outFile.cd() 
	for tree in outputTrees:
		tree.Write() 

	print( "All done!" )	
	
	return 0 
	
	
if __name__== "__main__":
	main()