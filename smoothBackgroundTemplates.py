# Script written by Rachel Hyneman, 2019 
# Contact: rachel.hyneman@cern.ch 

import numpy as np
import ROOT 
import sys 
sys.path.append('inc/')
from utils import generateBackgroundWorkspace, getInputXAxis, makeBinning, makeGPtemplate


########### User Defined Options ##########

# These are all pulled from the inc/inputs.py file 

import inputs as userin 


###########################################
########### The Important Stuff ###########

def main():
	
	
	if( not hasattr(userin, 'templateHistoNames') ): 
		print("ERROR! No template histogram names given (templateHistoNames). Nothing to smooth!")
		return 
	
	templateHistoNames = userin.templateHistoNames
	nCategories = len(templateHistoNames) 
	
	#hasattr(userin, 'inDataSBFileName')
	
	inDataSBFileName = ""
	if( hasattr(userin, 'inDataSBFileName') ): inDataSBFileName = userin.inDataSBFileName
	
	sideBandRefNames = ""
	if( hasattr(userin, 'sideBandRefNames') ): sideBandRefNames = userin.sideBandRefNames
	
	if ( not not inDataSBFileName ): 
		if ( len(sideBandRefNames) != nCategories ): 
			print( 	"WARNING - Input data sidebands file has been provided, but there are not enough sideband histograms passed. Will NOT include side bands!" )
			inDataSBFileName = "" 

	categoryNames = ""	
	if( hasattr(userin, 'categoryNames') ): categoryNames = userin.categoryNames
	if ( len(categoryNames) != nCategories ): 
		print( 	"WARNING - The number of category names passed is not the same as the number of background templates passed! Will ignore the given category names." )
		categoryNames = templateHistoNames 

	hyperParFileName = ""
	if( hasattr(userin, 'hyperParFileName') ): hyperParFileName = userin.hyperParFileName 
	if( not not hyperParFileName ): 
		print("INFO: Opening hyper-parameter file ",hyperParFileName)
		hyperParFile = ROOT.TFile.Open(hyperParFileName)

	outTemplateFile = ROOT.TFile(userin.outTemplateFileName,"RECREATE") 
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasStyle.C") 
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasUtils.C") 
	ROOT.SetAtlasStyle()

	for iCat in range(nCategories): 

		### Setup all inputs before applying GPR 
	
		inHistName = templateHistoNames[ iCat ]
		if( len(categoryNames) ): catName = categoryNames[ iCat ] 
		else: catName = inHistName
	
		print( "Applying GPR Smoothing to Category: ", catName )

		# Get the input template histogram 

		inFile = ROOT.TFile.Open(userin.inFileName,"READ")
		inHisto = inFile.Get(inHistName)
			
		if( not inHisto ): 
			print( "ERROR - Template '"+inHistName+"' not found!! Check template name and file contents." )
			continue 
		
		nRebin = 1 
		if( hasattr(userin, 'nRebin') ): nRebin = userin.nRebin
		if( nRebin != 1 ): inHisto.Rebin( nRebin )
	
		xMin = inHisto.GetBinLowEdge(1)
		xMax = inHisto.GetBinLowEdge( inHisto.GetNbinsX()+1 )
	
		# Get the data sidebands from the file (if specified) 
		# Sidebands are NOT used for smoothing (they are only an option for plotting)

		if ( not not inDataSBFileName ): 
			inDataSBFile = ROOT.TFile(inDataSBFileName) 
			sideBandRefName = sideBandRefNames[iCat]
			sideBandRefHisto = inDataSBFile.Get( sideBandRefName )
			if ( not sideBandRefHisto ): 
				print( "ERROR - Sidebands histogram '"+sideBandRefName+"' not found!! Check histogram name and file contents." )
				continue 	
		else: # make a dummy histo if not looking at data sidebands 
			sideBandRefHisto = ROOT.TH1D("sideBandRefHisto","sideBandRefHisto",inHisto.GetNbinsX(),xMin,xMax)
			sideBandRefName = ""	

		# Get the kernel hyper-parameters from the input file 
			
		length_scale_bounds = [-1,-1]
		length_scale_slope_bounds = [-1,-1]
				
		if( not not hyperParFileName ): 
			hyperParTree = hyperParFile.Get( "HyperParArea_"+inHistName )
			hyperParTree.GetEntry(0)
			length_scale_bounds[0] =  hyperParTree.GetLeaf("length_scale_min").GetValue() 
			length_scale_bounds[1] =  hyperParTree.GetLeaf("length_scale_max").GetValue()
			length_scale_slope_bounds[0] =  hyperParTree.GetLeaf("length_scale_slope_min").GetValue()
			length_scale_slope_bounds[1] =  hyperParTree.GetLeaf("length_scale_slope_max").GetValue()
		
			if( length_scale_bounds[0] < 0.0 or length_scale_bounds[1] < 0.0 ):
				print("ERROR - length scale bounds are invalid. The optimization may have failed.")
				print("   Try providing bounds manually in the input file (length_scale_bounds) and commenting out the hyperParFileName. ")
				print("   Will skip this category!! ")
				break 
		elif( hasattr(userin, 'length_scale_bounds') ):
			length_scale_bounds = userin.length_scale_bounds 
			length_scale_slope_bounds = userin.length_scale_slope_bounds
			if( length_scale_bounds[0] < 0.0 or length_scale_bounds[1] < 0.0 ):
				print("ERROR - the length scale bounds specified in the input file (length_scale_bounds) are invalid.")
				print("   Make sure these values are greater than 0. ")
				print("   Will skip this category!! ")
				break 
		else: 
			print("ERROR - no hyper-parameter bounds or file (length_scale_bounds/hyperParFileName) are provided.")
			return  
		
		length_scale = length_scale_bounds[0] + 0.9*( length_scale_bounds[1] - length_scale_bounds[0] )
		length_scale_slope = length_scale_slope_bounds[0] + 0.9*( length_scale_slope_bounds[1] - length_scale_slope_bounds[0] )
	
		# Check remaining user inputs
	
		nBinsToPad = 0 
		if( hasattr(userin, 'nBinsToPad') ): nBinsToPad = userin.nBinsToPad
		
		if( hasattr(userin, 'whichKernel') ): whichKernel = userin.whichKernel
		else: print("ERROR! No kernel (whichKernel) specified in the input file!")

		if( hasattr(userin, 'whichGPMean') ): whichGPMean = userin.whichGPMean
		else: print("ERROR! No GP mean (whichGPMean) specified in the input file!")
		
		if( hasattr(userin, 'whichErrorTreatment') ): whichErrorTreatment = userin.whichErrorTreatment
		else: print("ERROR! No input error treatment (whichErrorTreatment) specified in the input file!")

		if( hasattr(userin, 'whichErrorOutput') ): whichErrorOutput = userin.whichErrorOutput
		else: print("ERROR! No output error treatment (whichErrorOutput) specified in the input file!")

		calculateSystematics = False
		systVarFrac = 0.0
		if( hasattr(userin, 'calculateSystematics') ): 
			calculateSystematics = userin.calculateSystematics
			if( hasattr(userin, 'systVarFrac') ): 
				systVarFrac = userin.systVarFrac
			else: 
				print("ERROR!! Systematic fits requested, but not systematic length scale variation (systVarFrac)")
				break
		else: 
			print("WARNING!! User has not specified whether to perform systematic GP fits (missing calculateSystematics)")
			print("          Will skip performing systematic fits. ")

	
		# Perform the GP fit to obtain the smoothed template 
		
		smoothedTemplates = makeGPtemplate( inHisto , 
											userin.whichKernel , 
											length_scale , 
											length_scale_bounds , 
											length_scale_slope , 
											length_scale_slope_bounds , 
											whichGPMean , 
											nBinsToPad , 
											whichErrorTreatment , 
											whichErrorOutput , 
											calculateSystematics , 
											systVarFrac )	
	
		outTemplateFile.cd() 
		bkgTemplate = smoothedTemplates[0] 
		bkgTemplate.Write() 	
		
		if( calculateSystematics ): 
			bkgTemplateSystUp = smoothedTemplates[1]
			bkgTemplateSystDown = smoothedTemplates[2]
			bkgTemplateSystErr = smoothedTemplates[3]
			bkgTemplateSystUp.Write() 
			bkgTemplateSystDown.Write() 
			
		### If specified, save the original background template and the data sidebands 

		saveInputHistograms = False
		if( hasattr(userin, 'saveInputHistograms') ): saveInputHistograms = userin.saveInputHistograms
		else: 
			print("WARNING!! User has not specified whether to save input histograms (missing saveInputHistograms)")
			print("          Will NOT save the input histograms. ")
	
		if( userin.saveInputHistograms ): 
			inHisto.Write() 
			if ( not not inDataSBFileName ): sideBandRefHisto.Write() 
	
		########## The remainder is all plot styling!! 
	
		### Lost of Details to Make Nice Plots!  

		canvasName = "canvas_"+inHistName
		canvas = ROOT.TCanvas(canvasName,canvasName,600,600) 
		canvas.cd() 
		upperPad = ROOT.TPad("upperPad", "upperPad", 0.0, 0.3, 1.0, 1.0)
		lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.0, 0.0, 1.0, 0.3)
		upperPad.Draw() 
		upperPad.SetBottomMargin(0.02)
		lowerPad.Draw() 
		lowerPad.SetTopMargin(0.02) ;
		lowerPad.SetBottomMargin(0.3) ;

		# The upper pad 

		canvas.cd() 
		upperPad.cd() 
	
		yMax = 1.35*inHisto.GetMaximum()
		leg = ROOT.TLegend(0.65,0.65,0.89,0.89)
		leg.SetBorderSize(0)
		leg.SetFillStyle(-1)

		if ( not not sideBandRefName ): 
			sideBandRefHisto.SetMarkerColor(1)
			sideBandRefHisto.SetMarkerStyle(8)
			sideBandRefHisto.SetLineColor(1)
			sideBandRefHisto.SetLineWidth(1) 
			sideBandRefHisto.SetMinimum(0)
			sideBandRefHisto.SetMaximum(yMax)
			sideBandRefHisto.GetYaxis().SetTitle("Events / Bin")
			sideBandRefHisto.GetYaxis().SetTitleSize(0.055)
			sideBandRefHisto.GetXaxis().SetLabelSize(0.0)
			sideBandRefHisto.Draw("ep")
			leg.AddEntry(sideBandRefHisto,"Data Sidebands","p")

		bkgTemplate.SetLineColor(601)
		bkgTemplate.SetLineWidth(2)
		bkgTemplate.SetMarkerColor(601)
		bkgTemplate.SetMarkerSize(0)
		bkgTemplate.SetFillColorAlpha(601,0.3)
		bkgTemplate.SetFillStyle(3003)
		bkgTemplate.SetMinimum(0)
		bkgTemplate.SetMaximum(yMax)
		bkgTemplate.GetXaxis().SetTitleSize(0.0)
		bkgTemplate.GetXaxis().SetLabelSize(0.0)
		bkgTemplate.GetYaxis().SetTitle("Events / Bin")
		bkgTemplate.GetYaxis().SetTitleSize(0.055)
		bkgTemplate.Draw("e4 same")
		leg.AddEntry(bkgTemplate,"GPR Smoothed Template")
		
		if( userin.calculateSystematics ): 
			bkgTemplateSystErr.SetLineColor(870)
			bkgTemplateSystErr.SetLineStyle(1)
			bkgTemplateSystErr.SetLineWidth(2)
			bkgTemplateSystErr.SetMarkerColor(870)
			bkgTemplateSystErr.SetMarkerSize(0)
			bkgTemplateSystErr.Draw("ehist same")
			leg.AddEntry(bkgTemplateSystErr,"GPR Systematic Errors")
			
		inHisto.SetLineColor(633)
		inHisto.SetLineWidth(2)
		inHisto.SetMarkerSize(0)
		inHisto.SetMinimum(0)
		inHisto.SetMaximum(yMax)
		inHisto.GetYaxis().SetTitle("Events / Bin")
		inHisto.GetYaxis().SetTitleSize(0.055)
		inHisto.GetXaxis().SetLabelSize(0.0)
		inHisto.Draw("ehist same")
		leg.AddEntry(inHisto,"Original Template")	
		
		bkgTemplateCentral = bkgTemplate.Clone("bkgTemplateCentral")
		bkgTemplateCentral.SetLineColor(601)
		bkgTemplateCentral.SetLineWidth(2)
		bkgTemplateCentral.SetMarkerColor(601)
		bkgTemplateCentral.SetMarkerSize(0)
		bkgTemplateCentral.SetFillStyle(0)
		bkgTemplateCentral.Draw("hist same")
	
		leg.Draw() 	

		ROOT.ATLAS_LABEL(0.23,0.85)
		ROOT.myText(0.34,0.85,1,"Internal")
		ROOT.myText(0.23,0.80,1,"GPR Smoothed Template",0.04)
		ROOT.myText(0.23,0.76,1,catName,0.04)
	
		canvas.Update()

		# The lower pad 
		#    NOTE: If data sidebands are provided, will plot the difference of the original and 
		#    smoothed templates in reference to the data sidebands. Otherwise, will plot only the
		#    difference of the GPR smoothed template to the original. 

		canvas.cd() 
		lowerPad.cd()

		refLine = ROOT.TH1I("refLine","refLine",1,xMin,xMax)
		refLine.SetBinContent(1,0)
		refLine.SetLineColor(1)
		refLine.SetLineWidth(1)
		refLine.SetLineStyle(7)
		refLine.SetMinimum(-1.09)
		refLine.SetMaximum(1.09)
		refLine.GetXaxis().SetTitle("M_{#gamma#gamma} [GeV]")
		refLine.GetXaxis().SetTitleSize(0.13)
		refLine.GetXaxis().SetTitleOffset(1.0)
		refLine.GetXaxis().SetLabelSize(0.09)
		refLine.GetYaxis().SetTitleSize(0.11)
		refLine.GetYaxis().SetTitleOffset(0.55)
		refLine.GetYaxis().SetLabelSize(0.09)
		refLine.Draw()

		if( not not sideBandRefName ): # Plot scaled residuals of both templates w.r.t. data sidebands 
			inHistoResidual = inHisto.Clone("inHistoResidual")
			inHistoResidual.Add(sideBandRefHisto,-1)
			inHistoResidual.Divide(sideBandRefHisto)
			inHistoResidual.SetLineColor(633)
			inHistoResidual.Draw("ehist same")
			bkgTemplateResidual = bkgTemplate.Clone("bkgTemplateResidual")
			bkgTemplateResidual.Add(sideBandRefHisto,-1)
			bkgTemplateResidual.Divide(sideBandRefHisto)
			bkgTemplateResidual.SetLineColor(601)
			bkgTemplateResidual.Draw("ehist same")
			refLine.GetYaxis().SetTitle("#frac{Template - Data}{Data}") 

		else: # Plot scaled residuals of GPR template w.r.t. original template	
			if( userin.calculateSystematics ): 
				bkgTemplateResSyst = bkgTemplateSystErr.Clone("bkgTemplateResSyst")
				bkgTemplateResSyst.Add(inHisto,-1)
				bkgTemplateResSyst.Divide(inHisto)
				bkgTemplateResSyst.SetLineWidth(0)
				bkgTemplateResSyst.SetLineColor(601)
				bkgTemplateResSyst.Draw("e3 same")
						
			bkgTemplateResidual = bkgTemplate.Clone("bkgTemplateResidual")
			bkgTemplateResidual.Add(inHisto,-1)
			bkgTemplateResidual.Divide(inHisto)
			bkgTemplateResidual.SetLineColor(601)
			bkgTemplateResidual.Draw("ehist same")
		
			refLine.GetYaxis().SetTitle("#frac{GPR - Original}{Original}")
					
		refLine.Draw("same")

		canvas.Update()
		canvas.Print("GPR_Smoothed_Plot_"+inHistName+".eps")
		#canvas.Close() 
	
		refLine.Delete() 
	
	### All done!

	print( "All done!" )
	


if __name__== "__main__":
	main()