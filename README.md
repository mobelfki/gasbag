This package is designed to apply Gaussian Process Regression to Monte Carlo or data
CR background-only templates in order smooth out statistical fluctuations. Note 
that this is NOT designed to do a full signal+background fit in a signal region. 

In order to run, you'll need the following packages: 
- ROOT 6 with PyROOT
- Python 2.7+ (2.7 recommended on lxplus)
- scikit-learn 0.20.2+
- scipy 1.1.0+ (Note: only 1.1.0 works on lxplus)
- numpy 1.14.2+

In order to set up all of these very quickly, just do "source setup_environment" in the GaSBaG directory. 

In order to smooth a template, you should first consider some features of your background. 
First: is your background smooth? If it has a lot of narrow features, then GP smoothing may not be ideal for your case. 
Second: is your background consistently falling, or does it have some significant rise an points? 
If it's not consistently falling, then you should use a Radial Basis Function (RBF) kernel. 
This kernel is flexible, and quite simple. The only hyper-parameter is the length scale, which dictates the minimum feature size of your template. 
If it's always (or almost walways) falling, you may want to use a Gibbs kernel. 
The Gibbs kernel is similar to the RBF, except that the length scale increases linearly as x increases. 
The rate at which the length scale increases is another hyper parameter. 

 



Applying GPR to Smooth an Existing Background Template: 
The python script "smoothBackgroundTemplates.py" will apply a GPR fit to the 
background templates, as specified in the file "inputs_SmoothBackgroundTemplates.py". 
Use the options here to specify the input file name, template names, 
hyper parameters, etc. The script will produce a template with identical binning to 
the input template. However, the GPR fitting can be somewhat slow if a very large 
number of bins are provided. In this case, one can change the "nRebin" variable to 
someting greater than one to merge bins together. This will grealy speed up the 
template making process. 
In addition, the script assumes the GPR prior (very rough aproximation of the 
background shape) is a falling exponential function. This may need to be changed 
for analyses which have significantly different background shapes. Note that the 
prior just needs to be a **very rough** approximation of the background shape; if one 
knew the exact analytical shape, then this script would not be necessary! 


Obtaining Optimal Hyper Parameters: 
The pyhton script "optimizeHyperPars.py" will apply many GPR fits with different 
hyper parameters to a smooth functional fit to the background templates, as 
specified in "inputs_OptimizeHyperPars.py". Use the options here to specify the 
input file name, template names, hyper parameters to be tested, etc. The available 
fit functions (for generating a smooth shape) are: 
- "PowerLaw" 
- "Exponential" 
- "ExpPoly2" 
- "ExpPoly3" 
- "Bern3" 
- "Bern4" 
- "Bern5"
If your background template is very different, then you may need to add an additional 
function to the code. Once the code has run, a 2D histogram will be produced mapping 
the 2D Gibbs hyper-parameter space tested. The red area indicates good hyper-parameters, 
while the blue area indicates not-as-good hyper-parameters. The "good" hyper 
parameters are chosen such that narrow features O~bin width are smoothed out by 
at least 33%, while wider features of O~expected signal width are smoothed out by 
no more than 25%. The latter criteria is to ensure that if some real, signal like 
feature is sculpted by the analysis cuts on the background shape, this real spurious 
signal will not be removed. 


