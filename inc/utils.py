import numpy as np
import ROOT 
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel 
import sys
sys.path.append('inc/')
from FallExpKernel import * 
from LinearErrorKernel import * 
from myFunctions import *


###########################################

### Generate the workspace containing the "background" function ###

def generateBackgroundWorkspace( wsName , whichFunction , range_Myy ):

	ws = ROOT.RooWorkspace(wsName)
	ws.factory('rooMyy[{0}, {1}]'.format(range_Myy[0], range_Myy[1]))
	ws.factory('RooM0[{0}, {1}]'.format(range_Myy[0], range_Myy[1]))
	
	### Obtain the background PDF from the myFunctions.py definitions
	myBkgFunctions = define_background_functions() 
	
	bkgStr = ""
	for funcDef in myBkgFunctions: 
		funcName = funcDef[0]
		if( whichFunction == funcName ): bkgStr = funcDef[1]
	
	if( not bkgStr ): 
		print("ERROR: Requesting an undefined background function (",whichFunction,")")
		print("Check that the ",whichFunction," function is defined in inc/myFunctions.py")
	
	bkgPDF = ws.factory(bkgStr)		
	ws.saveSnapshot("initial",ws.allVars())
	
	return ws 

###########################################
	
### Generate the workspace containing the signal shape ###

def generateSignalWorkspace( wsName , whichFunction , range_Myy ):

	ws = ROOT.RooWorkspace(wsName)
	ws.factory('rooMyy[{0}, {1}]'.format(range_Myy[0], range_Myy[1]))
	
	### Obtain the signal PDF from the myFunctions.py definitions
	mySigFunctions = define_signal_functions() 
	
	sigStr = ""
	for funcDef in mySigFunctions: 
		funcName = funcDef[0]
		if( whichFunction == funcName ): sigStr = funcDef[1]
	
	if( not sigStr ): 
		print("ERROR: Requesting an undefined signal function (",whichFunction,")")
		print("Check that the ",whichFunction," function is defined in inc/myFunctions.py")
	
	sigPDF = ws.factory(sigStr)
	
	ws.saveSnapshot("initial",ws.allVars())
	
	return ws 

###########################################

### A quick function to create histograms from numpy arrays 

def makeHistoFromArray( inputArray , outputHisto , inputError = np.array('i') ): 
	nBins = outputHisto.GetNbinsX() 
	if( len(inputArray) != nBins ): 
		print("WARNING in makeHistoFromArray")
		print("Number of bins in histogram does not match length of input array!")
		print("Returning empty histogram.") 
		return 
	for iBin in range(nBins): 
		outputHisto.SetBinContent( iBin+1 , inputArray[iBin] ) 
		if( inputError.shape ): outputHisto.SetBinError( iBin+1 , inputError[iBin] ) 

###########################################

### A quick function to create numpy arrays from histograms

def makeArrayFromHisto( inputHisto ): 
	nBins = inputHisto.GetNbinsX()
	outputArray = np.array([],'d')
	for iBin in range(nBins): 
		outputArray = np.append( outputArray , inputHisto.GetBinContent( iBin+1 ) )
	return outputArray
	
###########################################

### Get the x-axis (bin centers) and bin edges from the input template

def getInputXAxis( inputHisto ): 

	nBins = inputHisto.GetNbinsX()
	
	x_Myy = np.array([],'d') 
	for ibin in range(nBins): 
		x_Myy = np.append( x_Myy , inputHisto.GetBinCenter(ibin+1) )
	
	return x_Myy


def makeBinning( inputHisto ): 

	nBins = inputHisto.GetNbinsX()
	
	xBins = np.array([],'d') 
	for ibin in range(nBins+1): 
		xBins = np.append( xBins , inputHisto.GetBinLowEdge(ibin+1) )
	
	myBins = ROOT.RooBinning(nBins,xBins,"myBins")
	
	return myBins 
	

###########################################

### Smooth the input template using the GP fit 


def makeGPtemplate( 	inHisto , 
						whichKernel , 
						length_scale , 
						length_scale_bounds , 
						length_scale_slope , 
						length_scale_slope_bounds , 
						whichGPMean , 
						nBinsToPad , 
						whichErrorTreatment , 
						whichErrorOutput , 
						calculateSystematics , 
						systVarFrac ):

	# Determine which error treatment will be used 
	
	useLinearErrorKernel = False
	useWhiteNoiseKernel = False
	if( whichErrorTreatment == 2 ): useLinearErrorKernel = True 
	if( whichErrorTreatment == 4 ): useWhiteNoiseKernel = True 

	useGPRFitErrors = False 
	useOriginalTemplateError = False 
	usePoissonError = False 
	
	if( whichErrorOutput == 1 ): useOriginalTemplateError = True 
	if( whichErrorOutput == 2 ): useGPRFitErrors = True 
	if( whichErrorOutput == 3 ): usePoissonError = True  

	# Get some information from the input template  

	inHistName = inHisto.GetName() 
 	
	if( nBinsToPad > 0 ): inHisto = padTemplateEdges( inHisto , nBinsToPad ) 

	x_Myy = getInputXAxis(inHisto) # the bin centers (x values used in the fitting)
	myBins = makeBinning(inHisto) # the bin edges (for defining histograms)
	
	xMin = myBins.lowBound() 
	xMax = myBins.highBound()
	
	nBins = inHisto.GetNbinsX()
	nEvts = inHisto.GetSumOfWeights() 

	myyHistArray = makeArrayFromHisto( inHisto )

	# Make a numpy array of the error bars from the input template histograms 

	myyErrs = np.array([],'d')
	for ibin in range( inHisto.GetNbinsX() ): 
		if( whichErrorTreatment == 3 ): 
			myyErrs = np.append( myyErrs , np.sqrt( inHisto.GetBinContent( ibin+1 ) ) )
		else:
			if( np.isnan( inHisto.GetBinError( ibin+1 ) ) ): 
				inHisto.SetBinError( ibin+1 , myyErrs[-1] )
				myyErrs = np.append( myyErrs , myyErrs[-1] )
			elif( inHisto.GetBinError( ibin+1 ) < 1e-5 ): 
				myyErrs = np.append( myyErrs , 1e-5 )
			else: myyErrs = np.append( myyErrs , inHisto.GetBinError( ibin+1 ) )
			

	### Define the GPR Prior (should be reasonably similar to the input template shape) 
	
	if( whichGPMean != "Flat" ):
		priorBkgWS = generateBackgroundWorkspace( "priorWS_"+inHistName , whichGPMean , [xMin,xMax] ) 
		templateRooHist = ROOT.RooDataHist("templateRooHist_"+inHistName, "templateRooHist_"+inHistName, ROOT.RooArgList(priorBkgWS.obj("rooMyy")), inHisto) 	
		nll = priorBkgWS.pdf('bkg').createNLL(templateRooHist)
		fitter = ROOT.RooMinimizer(nll)
		fitter.setEps(1E-3 / 0.001)
		fitter.setStrategy(1)
		fitter.setPrintLevel(-2)
		fitter.setOffsetting(True)
		fitter.minimize("Minuit2") 	
		priorHisto = priorBkgWS.obj('bkg').createHistogram("h_prior_"+inHistName,priorBkgWS.obj("rooMyy"),ROOT.RooFit.Binning(myBins)) 
		priorHisto.Scale( inHisto.Integral() / priorHisto.Integral() )
		myBaseLine = makeArrayFromHisto( priorHisto ) 
	else: myBaseLine = np.zeros(nBins)
	

	### Get estimates of different hyper-parameter bounds from the input template 

	# Set bounds on the scaling of the GPR template (basically a maximum y-range) 

	const_low = 10.0**-5
	const_hi = nEvts * 10.0
	
	# Set bounds on the magnitude of the GPR error bars 

	errConst0 = np.mean(myyErrs)**2
	errConst_low = 0.50*np.min(myyErrs)**2
	errConst_hi = 2.0*np.max(myyErrs)**2
	
	# Estimate the (decreasing) slope of the magnitude of the GPR error bars 

	slope_est0 = ( ( np.max(myyErrs)-myyErrs[-1] ) / np.max(myyErrs) ) / ( x_Myy[-1] - x_Myy[0] )
	if( slope_est0 < 10**-5 ): slope_est0 = 10**-4
	slope_est_low = 0.1*slope_est0
	slope_est_hi = 10.0*slope_est0 
		
	
	alphaVals = myyErrs.copy()	
	alphaVals = alphaVals**2
		
	### Define the GPR Kernel 
	
	kernel = ConstantKernel(constant_value=1.0, constant_value_bounds=(const_low,const_hi))

	if( whichKernel=="Gibbs" ): 
		kernel *= ( Gibbs(l0=length_scale,l0_bounds=(length_scale_bounds[0],length_scale_bounds[1]),
						  l_slope=length_scale_slope,l_slope_bounds=(length_scale_slope_bounds[0],length_scale_slope_bounds[1])) )
	
	
	elif( whichKernel=="RBF" ): 
		kernel *= RBF(length_scale=length_scale,length_scale_bounds=(length_scale_bounds[0],length_scale_bounds[1])) 	   	


	if( useLinearErrorKernel ): 
		kernel += ( ConstantKernel(constant_value=1.0, constant_value_bounds=(1.0,1.0))  * 
				   	LinearNoiseKernel(noise_level=errConst0,noise_level_bounds=(errConst_low,errConst_hi),
				   					  b=slope_est0,b_bounds=(slope_est_low,slope_est_hi)) )		
# 		kernel += ( ConstantKernel( constant_value=errConst0 , constant_value_bounds=(errConst_low,errConst_hi) )  * 
# 				   	LinearNoiseKernel(noise_level=0.9,noise_level_bounds=(1e-9,1.0),
# 				   					  b=1e-5,b_bounds=(1e-9,1.0)) )		
		alphaVals = (1e-9)*np.ones(len(myyErrs),'d')
	
	
	elif( useWhiteNoiseKernel ):
		kernel += ( ConstantKernel(constant_value=1.0, constant_value_bounds=(1.0,1.0)) * 
				WhiteKernel(noise_level=errConst0,noise_level_bounds=(errConst_low,errConst_hi)) )		
		#kernel += ( ConstantKernel(constant_value=errConst0, constant_value_bounds=(errConst_low,errConst_hi)) * 
		#				WhiteKernel(noise_level=0.1, noise_level_bounds=(1e-10, 2.0)) )
		alphaVals = (1e-9)*np.ones(len(myyErrs),'d')		


	### Fit the GPR Kernel to the input template 
	
	gpr = GaussianProcessRegressor( kernel=kernel , n_restarts_optimizer = 5 , alpha=2*alphaVals , normalize_y=False ) 
	gpr.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )
			
	# Check to see if GPR Prediction is effectively the same as the prior 
	# If so, re-do the GPR fitting, but with a flat prior instead of functional prior 
	
	gprPrediction, gprErr = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_std=True )
	
	testerGPhisto = ROOT.TH1D( "testerGP" , "testerGP" , nBins , myBins.array() ) 
	makeHistoFromArray( gprPrediction , testerGPhisto , gprErr )
	
	testerPriorhisto = ROOT.TH1D( "testerPrior" , "testerPrior" , nBins , myBins.array() ) 
	makeHistoFromArray( myBaseLine , testerPriorhisto , np.sqrt(myBaseLine) ) #np.sqrt(myBaseLine) np.zeros(len(myBaseLine))
	
	chi2_ndof = testerGPhisto.Chi2Test( testerPriorhisto , "WW CHI2/NDF" )

	#if( np.sqrt( (gprPrediction*gprPrediction).sum() )/ myyHistArray.sum() < 0.0001 ): 
	if( chi2_ndof < 0.1 ): 
		myBaseLine = np.zeros( len(myyHistArray) ) 
		gpr = GaussianProcessRegressor( kernel=kernel , n_restarts_optimizer = 3 , alpha=2*alphaVals , normalize_y=False ) 
		gpr.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )	
		gprPrediction, gprErr = gpr.predict( x_Myy.reshape(len(x_Myy),1) , return_std=True )

	gprError = np.copy( gprErr )
	gprPrediction = gprPrediction + myBaseLine 	
	
	### Determine the error bars based on requested error treatment 
	
	outputErrorBars = np.zeros(len(gprPrediction),'d')
	
	# Use the GPR template's linear error kernel for errors 
	if( useGPRFitErrors ): outputErrorBars = gprError.copy()
	
	# Use the original template's errors 
	if( useOriginalTemplateError ): outputErrorBars = myyErrs.copy()
	
	# Calculate Poisson errors 
	if( usePoissonError ): 
		for i in range(len(gprPrediction)): 
			outputErrorBars[i] = np.sqrt( gprPrediction[i] ) 
	
	### If using padding, need to define un-padded x-axis 
	
	outBins = np.array([],"d")
	nBins = nBins - 2*nBinsToPad 
	for i in range(nBins+1): outBins = np.append( outBins , myBins.array()[i+nBinsToPad] ) 

	### Calculate Systematic Errors by Varying the Length Scale 
	
	gprSystErrors = np.zeros(len(gprPrediction),'d')
	
	if( calculateSystematics ):
	
		# Define the systematic kernels (vary the length scale by some fraction, keeping the other parameters constant
		
		theta_nom = gpr.kernel_.theta
		theta_up = []
		theta_down = []
		for ipar,hyperpar in enumerate(gpr.kernel_.hyperparameters): 
			theta_up.append( theta_nom[ipar] )
			theta_down.append( theta_nom[ipar] )
			if( ( 'l0' in hyperpar.name ) or ( 'length_scale' in hyperpar.name ) ): 
				theta_up[ipar] += np.log(1.0 + systVarFrac) 
				theta_down[ipar] += np.log(1.0 - systVarFrac) 
		
		kernel_systUp = gpr.kernel_.clone_with_theta(theta_up)
		kernel_systDown = gpr.kernel_.clone_with_theta(theta_down)
		
		# Perform the fits using the systematic kernels 
			
		gpr_systUp = GaussianProcessRegressor( kernel=kernel_systUp , alpha=2*alphaVals , optimizer=None , n_restarts_optimizer = 1 )
		gpr_systDown = GaussianProcessRegressor( kernel=kernel_systDown , alpha=2*alphaVals , optimizer=None , n_restarts_optimizer = 1 )
		
		gpr_systUp.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )
		gpr_systDown.fit( x_Myy.reshape(len(x_Myy),1) , (myyHistArray-myBaseLine).ravel() )
		
		gprPrediction_systUp = gpr_systUp.predict( x_Myy.reshape(len(x_Myy),1) )
		gprPrediction_systUp = gprPrediction_systUp + myBaseLine
		
		gprPrediction_systDown = gpr_systDown.predict( x_Myy.reshape(len(x_Myy),1) )
		gprPrediction_systDown = gprPrediction_systDown + myBaseLine
		

		# Calculate the error bars (maximum of the two variations)
		
		gprHiErrors = np.zeros(len(gprPrediction),'d') 
		gprLowErrors = np.zeros(len(gprPrediction),'d')
		for i in range(len(gprPrediction)): 
			gprHiErrors[i] = np.abs( gprPrediction[i] - np.max([gprPrediction_systUp[i],gprPrediction_systDown[i]]) )
			gprLowErrors[i] = np.abs( gprPrediction[i] - np.min([gprPrediction_systUp[i],gprPrediction_systDown[i]]) )
			gprSystErrors[i] += np.max([gprHiErrors[i],gprLowErrors[i]]) 
			outputErrorBars[i] += gprSystErrors[i] 
		
		# Get rid of padded range 
		
		temp_gprError = outputErrorBars.copy()
		temp_gprPred = gprPrediction.copy()
		
		if( nBinsToPad > 0 ):
			gprPrediction_systUp = gprPrediction_systUp[nBinsToPad:-nBinsToPad]
			gprPrediction_systDown = gprPrediction_systDown[nBinsToPad:-nBinsToPad]
			gprSystErrors = gprSystErrors[nBinsToPad:-nBinsToPad]
			
			temp_gprPred = temp_gprPred[nBinsToPad:-nBinsToPad] 
			temp_gprError = temp_gprError[nBinsToPad:-nBinsToPad] 
		
		### Create output histograms with systematics info 
		
		# Histogram with the systematic errors only (for plotting) 
		templateName = "GPR_Smoothed_"+inHistName+"_systErr" 
		bkgTemplateSystErr = ROOT.TH1D( templateName , templateName , nBins , outBins ) 
		makeHistoFromArray( temp_gprPred, bkgTemplateSystErr , gprSystErrors )
				
		# Below are the histograms of the GPR fits with systematically varied length scale 
		templateName = "GPR_Smoothed_"+inHistName+"_systUp" 
		bkgTemplateSystUp = ROOT.TH1D( templateName , templateName , nBins , outBins ) 
		makeHistoFromArray( gprPrediction_systUp, bkgTemplateSystUp , temp_gprError )
				
		templateName = "GPR_Smoothed_"+inHistName+"_systDown" 
		bkgTemplateSystDown = ROOT.TH1D( templateName , templateName , nBins , outBins ) 
		makeHistoFromArray( gprPrediction_systDown, bkgTemplateSystDown , temp_gprError )
		
	
	### Make the output histogram with the smoothed template 	
	
	# Get rid of padded range 
	if( nBinsToPad > 0 ):
		gprPrediction = gprPrediction[nBinsToPad:-nBinsToPad]
		outputErrorBars = outputErrorBars[nBinsToPad:-nBinsToPad]
	
	# Define the histogram 
	
	templateName = "GPR_Smoothed_"+inHistName
	gprPredictionHisto = ROOT.TH1D( templateName , templateName , nBins , outBins ) 
	makeHistoFromArray( gprPrediction , gprPredictionHisto , outputErrorBars )
		
	whatToReturn = [ gprPredictionHisto ]
	if( calculateSystematics ): 
		if( 'bkgTemplateSystUp' in locals() ): whatToReturn.append( bkgTemplateSystUp )
		if( 'bkgTemplateSystDown' in locals() ): whatToReturn.append( bkgTemplateSystDown )	
		if( 'bkgTemplateSystErr' in locals() ): whatToReturn.append( bkgTemplateSystErr )
	
		
	# Plots for debugging 
# 	c = ROOT.TCanvas("c","c",600,600)
# 	inHisto.SetLineColor(1)
# 	inHisto.Draw("ehist")
# 	priorHisto.SetLineColor(2)
# 	priorHisto.Draw("hist same")
# 	gprPredictionHisto.SetLineColor(4)
# 	gprPredictionHisto.Draw("ehist same")
# 	import pdb; pdb.set_trace() 
			
	
	return whatToReturn

	
	
###########################################
	
### Artificially pad the edges of the input template ##### 

def padTemplateEdges( inHisto , nBinsToPad ): 
	
	# Get some information from the input template  

	inHistName = inHisto.GetName()

	x_Myy = getInputXAxis(inHisto) # the bin centers (x values used in the fitting)
	myBins = makeBinning(inHisto) # the bin edges (for defining histograms)
	
	xMin = myBins.lowBound() 
	xMax = myBins.highBound()
	
	nBins = inHisto.GetNbinsX()
	nEvts = inHisto.GetEntries() 
	
	# Determine the new x-range after padding, as well as the range of the fit used to extrapolate 
	
	binWidthLow = inHisto.GetBinWidth(1)
	binWidthHigh = inHisto.GetBinWidth(nBins) 
	
	xMinPadded = xMin - binWidthLow*nBinsToPad 
	xMaxPadded = xMax + binWidthLow*nBinsToPad
	
	xMinFit = xMin + binWidthLow*nBinsToPad
	xMaxFit = xMax - binWidthLow*nBinsToPad
	
	# Estimate the fit parameters (to improve fit convergence)
	
	guess_b = ( inHisto.GetBinContent(inHisto.GetNbinsX()) - inHisto.GetBinContent(1) ) / ( xMax - xMin )
	guess_c = -1.0*guess_b*xMin + inHisto.GetBinContent(1) 
	
	fitLow =  ROOT.TF1("fitLow","pol1",xMin,xMinFit)
	fitHigh =  ROOT.TF1("fitHigh","pol1",xMaxFit,xMax)
	
	fitResultLow = inHisto.Fit("fitLow","SR")
	fitResultHigh = inHisto.Fit("fitHigh","SR+") 
	
	mLow = fitResultLow.Parameter(1)
	bLow = fitResultLow.Parameter(0)

	mHigh = fitResultHigh.Parameter(1)
	bHigh = fitResultHigh.Parameter(0)	
	
	errLow = 0.0 
	errHigh = 0.0 
	for ibin in range(nBinsToPad): 
		errLow += inHisto.GetBinError(ibin+1)
		errHigh += inHisto.GetBinError( inHisto.GetNbinsX() - ibin )
	errLow = errLow / (1.0*nBinsToPad)
	errHigh = errHigh / (1.0*nBinsToPad)
	
	# Make a new, padded histogram 
	
	newBins = np.array([],"d")
	for ibin in range(nBinsToPad): 
		newBins = np.append( newBins , xMin - (nBinsToPad-ibin)*binWidthLow )
	for ibin in range(nBins): 
		newBins = np.append( newBins , myBins.array()[ibin] )
	for ibin in range(nBinsToPad+1): 
		newBins = np.append( newBins , xMax + ibin*binWidthHigh )
					
	paddedHisto = ROOT.TH1D(inHistName+"_pad",inHistName+"_pad",len(newBins)-1,newBins)
	
	for ibin in range(paddedHisto.GetNbinsX()): 
		binx = paddedHisto.GetBinCenter(ibin+1)
		if( ibin < nBinsToPad ): 
			if( mLow*binx+bLow > 0.0 ): yVal = mLow*binx+bLow
			else: yVal = 0.0
			paddedHisto.SetBinContent( ibin+1 , yVal )
			paddedHisto.SetBinError( ibin+1 , errLow )
		elif( ibin >= nBinsToPad and ibin < (nBins+nBinsToPad) ): 
			paddedHisto.SetBinContent( ibin+1 , inHisto.GetBinContent(ibin-nBinsToPad+1) )	
			paddedHisto.SetBinError( ibin+1 , inHisto.GetBinError(ibin-nBinsToPad+1) )	
		elif( ibin >= (nBins+nBinsToPad) ): 
			if( mHigh*binx+bHigh > 0.0 ): yVal = mHigh*binx+bHigh
			else: yVal = 0.0
			paddedHisto.SetBinContent( ibin+1 , yVal )	
			paddedHisto.SetBinError( ibin+1 , errHigh )
	
	return paddedHisto 


###########################################
	
### Apply Poisson fluctuations to the input template 

def fluctuateTemplate( inHisto ): 

	# Get some information from the input template  

	inHistName = inHisto.GetName()

	nBins = inHisto.GetNbinsX()
	myBins = makeBinning(inHisto) # the bin edges (for defining histograms)
	myBinsArray = np.array([],"d")
	for i in range(nBins+1): myBinsArray = np.append( myBinsArray , myBins.array()[i] ) 
	
	# Make the new histogram 
	
	newHistoName = inHistName + "_fluc"
	newHisto = ROOT.TH1D( newHistoName , newHistoName , nBins , myBinsArray )
	
	for ibin in range(nBins): 
		sqErr = inHisto.GetBinError( ibin+1 )**2
		fluc = ROOT.RooRandom.randomGenerator().Poisson( sqErr ) - sqErr
		newBinContent = inHisto.GetBinContent( ibin+1 ) + fluc 
		newHisto.SetBinContent( ibin+1 , newBinContent )
		newHisto.SetBinError( ibin+1 , inHisto.GetBinError( ibin+1 ) )
		
	return newHisto 


###########################################
	
### Perform a Signal+Background Fit on a Template 


def performSBFit( inTemplate , basisSigWS , sigYield , basisBkgWS , bkgYield ): 

	### Construct a signal+bkg fit to the smoothed template to evaluate new fitted signal 
	testSmoothingWS = ROOT.RooWorkspace("testSmoothingWS")
	
	sigPDFNames = basisSigWS.allPdfs()
	pdfiter = sigPDFNames.createIterator()
	whichpdf = pdfiter.Next()	
	sigPDF_name = whichpdf.GetName()
	
	bkgPDF_name = basisBkgWS.GetName()[4:-2]

	getattr(testSmoothingWS,'import')( basisSigWS.pdf(sigPDF_name) ) 
	getattr(testSmoothingWS,'import')( basisBkgWS.pdf("bkg") ) 
	
	testSmoothingWS.factory('Ns[{0}, {1}, {2}]'.format(sigYield, -bkgYield , bkgYield))
	testSmoothingWS.factory('Nb[{0}, 0, {1}]'.format(bkgYield, 10.*bkgYield))
	testSmoothingWS.factory("SUM:sbModel(Ns*"+sigPDF_name+",Nb*bkg)")

	allPdfVars = testSmoothingWS.allVars() 
	nonConstVars = ["rooMyy","Ns","Nb"] 
	variter = allPdfVars.createIterator()
	whichvar = variter.Next()
	while whichvar:
		if( ( not whichvar.GetName() in nonConstVars ) and ( not bkgPDF_name in whichvar.GetName() ) ): 
			testSmoothingWS.var( whichvar.GetName() ).setConstant(ROOT.kTRUE) 
		whichvar = variter.Next() 

	dummyRooHist = ROOT.RooDataHist("dummyGPRooHist", "dummyGPRooHist", ROOT.RooArgList(testSmoothingWS.obj("rooMyy")), inTemplate )

	nll = testSmoothingWS.obj('sbModel').createNLL(dummyRooHist)
	fitter = ROOT.RooMinimizer(nll)
	fitter.setEps(1E-3 / 0.001)
	fitter.setStrategy(1)
	fitter.setPrintLevel(-2)
	fitter.setOffsetting(True)
	fitter.minimize("Minuit2")
	fitResultSigBkg = fitter.save("fitResultSigBkg", "fitResultSigBkg")

	fittedSignal = testSmoothingWS.obj('Ns').getVal()	
	
	parShortNames = [ "b" , "c" , "d" , "e" , "f" ] 
	bkgFitParams = []
	for par in parShortNames: 
		parName = testSmoothingWS.GetName()[9:] + "_" + par 
		par_exists = testSmoothingWS.var(parName)
		if( not not par_exists ): bkgFitParams.append( testSmoothingWS.var(parName).getValV() ) 
		else: bkgFitParams.append( 0.0 )
		
	# Plots for debugging 
# 	c = ROOT.TCanvas("c","c",600,600)
# 	frame = testSmoothingWS.obj("rooMyy").frame() 
# 	dummyRooHist.plotOn(frame)
# 	testSmoothingWS.obj('sbModel').plotOn(frame)
# 	frame.Draw() 
# 	c.Update() 
# 	import pdb; pdb.set_trace() 
		
	del dummyRooHist 
	del testSmoothingWS
	
	return( fittedSignal , fitResultSigBkg.status() , bkgFitParams )		
	
	