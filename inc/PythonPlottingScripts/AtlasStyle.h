//
//   @file    AtlasStyle.h         
//   
//            ATLAS Style, based on a style file from BaBar
//
//
//   @author M.Sutton
// 
//   Copyright (C) 2010 Atlas Collaboration
//
//   $Id: AtlasStyle.h 110319 2013-08-16 13:31:01Z amccarn $

#ifndef  __ATLASSTYLE_H
#define __ATLASSTYLE_H

#include "TStyle.h"

void SetAtlasStyle();

TStyle* AtlasStyle(); 

#endif // __ATLASSTYLE_H
