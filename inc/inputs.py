# The name of the input file containing the input MC template 
inFileName = "example_template.root"

# The names of the background templates you want to smooth, contained in the file above. 
templateHistoNames = [ "myBkgHisto" ]

# A list of the names of the categories for which templates have been provided (optional)
#   If empty, will default to the above list of template names. This is just for plot labels.
categoryNames = [ "My Example Template" ]

# The name of the input file containing the data sidebands (optional)
#   This can be the same as the filename containing the bkg templates , or it can be blank
#   if the user does not want to look at the data sidebands
inDataSBFileName = "example_template.root"

# The name of the histograms containing the sideband data events (optional). This can be
# left empty. If used, the sidebands should correspond to the categories given above.Note
# that the sidebands are not used by the smoothing - this is just a plotting option.
sideBandRefNames = [ "TruthHisto" ] 
# 	The exampleHisto.root file contains a histogram generated from an exponential PDF. 
#	The TruthHisto histogram is this PDF without fluctuations. 

# How many bins to "pad" the input histogram with (optional). This artificially extends the
# input template by N bins on either side, using a linear extrapolation. This may improve
# the quality of the GP fit near the template edges.
nBinsToPad = 5

# Whether or not to calculate systematic error contribution by varying the length scale 
calculateSystematics = True 

# How much (in fractional form) to vary the length scale when doing systematic GP fits  
# i.e. to vary by 20%, set as 0.2 
systVarFrac = 0.2 

# How to determine the errors while doing the GPR fit 
# Options are: 
# 	1: Use the original errors (non-flexible) from the input template
# 		- This option is more generic, and is best for cases where the template x-axis is large 
# 	2: Use the linear error kernel to flexibly approximate Poisson errors in the fit 
#		- This option is designed for background which are SMOOTH and FALLING in a relatively narrow x-range 
# 	3: Calculate Poisson error bars for the input template (effectively ignore MC weights)
#		- This option may help templates made from multiple MC samples with very different weights, 
#		  which sometimes causes funny features in the template error bars
# 	4: Use WhiteKernel
#		- This option is best for templates which are very roughly flat 
whichErrorTreatment = 1

# How to determine the error bars of the smoothed template output
# Options are: 
# 	1: Use the original error bars from the input template
# 	2: Use the fitted error bars from the GP fit
# 	3: Calculate Poisson error bars 
whichErrorOutput = 1

# Which mean function to use (should be simple, but also should describe the template decently well)
# Can use any of the functions in inc/myFunctions.py, as well as "Flat" (for a flat line 
# normalized to the mean of the input template). 
whichGPMean = "Exponential" 

# Which kernel to use. The Gibbs kernel works well for falling ~exponential spectra. The 
# RBF kernel is more general (the length scale is constant), and should be used otherwise. 
whichKernel = "Gibbs"  # options = Gibbs, RBF

# The hyper-parameter ranges to be used, if not using the outputs of the hyper-par optimization. 
# If using the Gibbs kernel, provide both the 'length scale' and the 'length scale slope' 
# boundaries. If using the RBF kernel, only the 'length scale' bounds are needed. 
length_scale_bounds = [5,7]
length_scale_slope_bounds = [0.01,1.0]

# The number of bins to combine, if rebinning input histogram (optional, but can make the 
# GPR fit run faster if the input histogram has very fine binning) 
nRebin = 1

# The name of the output file containing the GPR-smoothed template
outTemplateFileName = "mySmoothedTemplate.root"

# Option to save the original templates (and data sidebands, if provided) in the output file 
saveInputHistograms = True 


########## Below are the settings for the hyper-parameter optimization ##########
#################################################################################

### BROKEN! Don't use this... 
# Whether or not to use python multiprocessing when performing the scan 
# useMultiprocessing = False


# How much smoothing of a narrow, "noise-like" feature is required 
# i.e. a value of 0.3 requires the feature to be smoothed by 30% 
# If this is set to 0, then the code will only scan for the upper bound. This will save 
# significant runtime, since fewer GP fits will be performed. Additionally, whether or not 
# a minimum amount of smoothing is required will not bias the fit results. 
narrowFeatureTolerance = 0.1

# How much smoothing of a wide, "signal-like" feature is acceptable 
# i.e. a value of 0.1 will allow the feature to be smoothed by 10% 
# This may also be given in terms of the statistical error on the signal yield by passing 
# a string with the format "#*sigma" (for example, "2*sigma" would set the tolerance on the 
# signal yield to be 2x the statistical error). In this case, the statistical error of the 
# signal yield will be calculated, and the hyper parameters will be constrained such that 
# the signal yield is not reduced by more than # times that statistical error 
#largeFeatureTolerance = "1*sigma"
largeFeatureTolerance = 0.1

# The name of the file containing the optimized Gibbs hyper-parameter ranges for each category
# (produced as an output by the optimization, then used as an input by the smoothing)
hyperParFileName = "myExampleHyperParOptimization.root"

# The possible parameters for the kernel that will be tested. If using the Gibbs kernel, 
# provide test values for both the 'length scale' and 'length scale slope' parameters. If 
# using the RBF kernel, then the 'length scale slope' values are un-needed. 
length_scales = [ 1 , 2 , 5 , 7 , 10 , 15 , 20 ]
length_scale_slopes = [ 0.001 , 0.01 , 0.1 , 1.0 ]


##### Options for extracting a "signal-like" feature from a RooFit file ##### 
# This defines the "maximum length scale" of the smoothing. If a real feature shows up in 
# the background-only sample (i.e. the backgrund is sculpted), then this shape should NOT 
# be smoothed out (since it isn't a fluctuation). 

# The name of the ROOT file containing the fitted signal shapes (from the signal parameterization code)
# If left blank, then the provided signal widths in the option below will be used instead
inSignalFileName = "myExampleSignalShapeWS.root"

# The name of the signal shape workspace in the signal ROOT file
inSignalWorkspaceName = "mySigWS"

# The names of the signal PDF shapes from the input RooFit file
signalFile_sigPDFNames = ["mySigPDF"]

# The names of the signal data sets (the RooData set associated with the signal shape).
signalFile_sigDataNames = ["mySigData"]

# The names of the signal yields (the number of signal events set associated with the signal shape)
# Note that this is not typically stored in the PDF for each category (hence the need for an explicit list)
signalFile_sigYieldNames = ["mySigYield"]

# The "nametags" of the signal parameters within the input RooFit workspace. Ideally, all 
# the variables corresponding to the same parameter (i.e. sigma, the signal width) have 
# mostly the same name, other than some indication of the category that variable 
# corresponds to. For example, the workspace may contain sigma parameters for each category 
# with names like "sigmaCBNom_SM_m125000_c#", where # = the category ID number. In this case, 
# the "nametag" corresponds to the common portion of the name of the variable for all 
# categories, in this example "sigmaCBNom_SM_m125000". 

signalFile_sigWidthNametag = "sigWidth"	# The signal width (~sigma)
signalFile_sigMeanNametag = "RooM0"		# The signal mean (~mu)
signalFile_sigObsNametag = "rooMyy"				# The observable (~m_yy for diphoton analyses)

##### Options for manually defining a "signal-like" feature ##### 
# Use the options below to manually define the "signal-like" feature used to constrain the
# amount of smoothing applied. Note that the above options will override those below. 

# The desired functional shape to use to model the "signal-like" feature. This should be 
# defined in the inc/myFunctions file 
whichSigShape = "CrystalBall"

# The fitted signal widths (fitted sigma_CB) for each category, from the signal parameterization code 
# Can also provide a single value to be used for all categories 
signalWidths = [ 	2.0 ]

# The expected signal position in Myy. This can be provided as a list (with one value for
# each category) or as a single value to be used for all categories. 
signalPositions = [ 125. ]

# The number of signal events (raw yield), or the percentage of injected signal-like events 
# to inject (i.e. 0.01 = the number of injected "signal" events will be equal to 1% of the 
# total number of events in each template). Use EITHER the yield or fraction - if both are 
# provided, the signalYields will be used, and the signalFractions will be ignored. These 
# can be provided as a list (with one value for each category) or as a single value to be 
# used for all categories. 			
signalYields = [ 200 ] 
#signalFractions = [ 0.02 ]


########## Below are the settings for signal injection toy tests ##########
###########################################################################

# The name of the output file containing the toy test results 
outToyTestFileName = "myExampleToyTests.root"

# The name of the output file containing the basis shape for the toy tests, for use when 
# building toy templates from an analytic function fit to the input template. This file is 
# needed when running many jobs of a few toys via batch. Otherwise, the analytic function 
# used to generate the toys may differ between jobs, giving strange results. To generate 
# this file, run "python3 toyTest.py --generateToyBasisFile". 
outToyBasisFileName = "myExample_ToyBasis.root"

# The number of toy tests to run. For each toy, poisson fluctuations will be randomly 
# applied to the background shape. 
nSignalInjectToys = 2

# Whether or not to use an analytic function as the "basis" of each toy, or the original 
# input template. It is recommended to use the analytic function, unless the template has 
# very high statistics. Otherwise, it will be hard to say whether discrepancies between 
# the GP fit and the original template are really due to mismodeling or statistical effects. 
useSmoothToys = True 


# The number of signal events (nSignalInject), or the percentage of signal-like events to 
# inject (i.e. 0.01 = the number of injected "signal" events will be equal to 1% of the 
# total number of events in each template). Use EITHER the yield or fraction - if both are 
# provided, the nSignalInject will be used, and the signalInjectFractions will be ignored. 
# These can be provided as a list (with one value for each category) or as a single value 
# to be used for all categories. Note that these values WILL overwrite the signal yield 
# saved in the input signal workspace (if used). If you want to use the expected signal 
# yield and you've provided a signal workspace file, comment both of these out! 
# nSignalInject = [ 0 ] 
# signalInjectFractions = [ 0.1 ]


