from sklearn.gaussian_process.kernels import *

def _convert_to_double(X):
    return np.ascontiguousarray(X, dtype=np.double)

class LinearNoiseKernel(StationaryKernelMixin, Kernel):
    """White kernel.
    The main use-case of this kernel is as part of a sum-kernel where it
    explains the noise-component of the signal. Tuning its parameter
    corresponds to estimating the noise-level.
    k(x_1, x_2) = noise_level(x_1) if x_1 == x_2 else 0  
    Parameters
    ----------
    noise_level : array, shape (n_samples_X, n_features)
        Parameter controlling the noise level (vector of same shape as X)
    """
    def __init__(self,noise_level=1.0,noise_level_bounds=(1e-5,1e5),b=1.0,b_bounds=(1e-5,1e5)):
        self.noise_level = noise_level
        self.noise_level_bounds = noise_level_bounds
        self.b = b
        self.b_bounds = b_bounds
        
    @property
    def hyperparameter_noise_level(self):
        return Hyperparameter("noise_level", "numeric", self.noise_level_bounds)

    @property
    def hyperparameter_b(self):
        return(Hyperparameter("b","numeric",self.b_bounds))

    def __call__(self, X, Y=None, eval_gradient=False):
        """Return the kernel k(X, Y) and optionally its gradient.
        Parameters
        ----------
        X : array, shape (n_samples_X, n_features)
            Left argument of the returned kernel k(X, Y)
        Y : array, shape (n_samples_Y, n_features), (optional, default=None)
            Right argument of the returned kernel k(X, Y). If None, k(X, X)
            if evaluated instead.
        eval_gradient : bool (optional, default=False)
            Determines whether the gradient with respect to the kernel
            hyperparameter is determined. Only supported when Y is None.
        Returns
        -------
        K : array, shape (n_samples_X, n_samples_Y)
            Kernel k(X, Y)
        K_gradient : array (opt.), shape (n_samples_X, n_samples_X, n_dims)
            The gradient of the kernel k(X, X) with respect to the
            hyperparameter of the kernel. Only returned when eval_gradient
            is True.
        """
        
        X = np.atleast_2d(X)
        
        if Y is not None and eval_gradient:
            raise ValueError("Gradient can only be evaluated when Y is None.")

        if Y is None:
        	K = np.zeros((X.shape[0],X.shape[0]))
        	for i in range(0,X.shape[0]):
        		K[i,i] = self.noise_level + (-1.0) * self.b * ( X[i][0] - X[0][0] ) 
        		if( K[i,i] < 0 ) : K[i,i] = 0.0 
        	
        	if eval_gradient:
        		def f(theta): return self.clone_with_theta(theta)(X, Y)
        		return K, _approx_fprime(self.theta, f, 1e-10)
        		return K, None
        	else: return K
        
        else: return np.zeros((X.shape[0], Y.shape[0]))
            

    def diag(self, X):
        """Returns the diagonal of the kernel k(X, X).
        The result of this method is identical to np.diag(self(X)); however,
        it can be evaluated more efficiently since only the diagonal is
        evaluated.
        Parameters
        ----------
        X : array, shape (n_samples_X, n_features)
            Left argument of the returned kernel k(X, Y)
        Returns
        -------
        K_diag : array, shape (n_samples_X,)
            Diagonal of kernel k(X, X)
        """
        
        K_diag = np.ones(X.shape[0])
        for i in range(0,X.shape[0]):
        	K_diag[i] = self.noise_level + (-1.0) * self.b * ( X[i][0] - 99.0 )
        
        return K_diag   
        

    def __repr__(self):
        return "{0}(noise_level={1},b={2})".format(self.__class__.__name__,self.noise_level,self.b)



# adapted from scipy/optimize/optimize.py for functions with 2d output
def _approx_fprime(xk, f, epsilon, args=()):
    f0 = f(*((xk,) + args))
    grad = np.zeros((f0.shape[0], f0.shape[1], len(xk)), float)
    ei = np.zeros((len(xk), ), float)
    for k in range(len(xk)):
        ei[k] = 1.0
        d = epsilon * ei
        grad[:, :, k] = (f(*((xk + d,) + args)) - f0) / d[k]
        ei[k] = 0.0
    return grad