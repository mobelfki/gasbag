# Script written by Rachel Hyneman, 2019 
# Contact: rachel.hyneman@cern.ch 

import numpy as np
import ROOT
import uproot 
import sys 
sys.path.append('inc/')
from utils import generateBackgroundWorkspace, generateSignalWorkspace, getInputXAxis, makeBinning, makeGPtemplate, define_background_functions, fluctuateTemplate, performSBFit 


########### User Defined Options ##########

# These are all pulled from the inc/inputs.py file 

import inputs as userin 


###########################################
########### The Important Stuff ###########

def main():
	
	if( not hasattr(userin, 'templateHistoNames') ): 
		print("ERROR! No template histogram names given (templateHistoNames). Nothing to smooth!")
		return 
	
	templateHistoNames = userin.templateHistoNames
	nCategories = len(userin.templateHistoNames) 

	categoryNames = ""	
	if( hasattr(userin, 'categoryNames') ): categoryNames = userin.categoryNames
	if ( len(categoryNames) != nCategories ): 
		print( 	"WARNING - The number of category names passed is not the same as the number of background templates passed! Will ignore the given category names." )
		categoryNames = templateHistoNames 

	inTemplateFile = ROOT.TFile.Open(userin.inFileName,"READ") 
	inToyTestFile = ROOT.TFile.Open(userin.outToyTestFileName,"READ") 
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasStyle.C") 
	ROOT.gROOT.LoadMacro("./inc/PythonPlottingScripts/AtlasUtils.C") 
	ROOT.SetAtlasStyle()
	
	
	for iCat in range(nCategories): 

		inHistName = templateHistoNames[ iCat ]
		if( len(categoryNames) ): catName = categoryNames[ iCat ] 
		else: catName = inHistName
		
		myBkgFunctions = define_background_functions() 

		print( "Plotting Toy Results for Category: ", catName )
		
		
		gprHistoVals = [] 
		toyHistoVals = [] 
		
		
		### First get the actual toy histos+fits 
		
# 		treeName = inHistName+"_ToyTests_gen"
# 		genTree = inToyTestFile.Get( treeName )
# 		
# 		nToys = genTree.GetEntries()
# 		for ientry in range(nToys): 
# 			genTree.GetEntry( ientry )


		treeName = inHistName+"_ToyTests_gen"
		inFile = uproot.open( userin.outToyTestFileName )
		genTree = inFile[treeName]
		
		binEdges = np.array( genTree['binning'].array()[0] , dtype='d' )
		nBins = len( binEdges ) - 1 
		
		avgGPhisto = ROOT.TH1D( catName+'_avgGP' , catName+'_avgGP' , int(nBins) , binEdges )
		avgToyhisto = ROOT.TH1D( catName+'_avgToy' , catName+'_avgToy' , int(nBins) , binEdges )
		
		residGPhisto = ROOT.TH1D( catName+'_resGP' , catName+'_resGP' , int(nBins) , binEdges )
		residToyhisto = ROOT.TH1D( catName+'_resToy' , catName+'_resToy' , int(nBins) , binEdges )
		
		truthHistName = inHistName+"_ToyBasis"
		rootFile = ROOT.TFile.Open(userin.outToyTestFileName)
		truthHisto = rootFile.Get(truthHistName)
		
		xMin = truthHisto.GetBinLowEdge(1)
		xMax = truthHisto.GetBinLowEdge(nBins+1)
		
		for ibin in range(nBins): 
		
			avgVal = np.median( genTree['GPR_histVals'].array()[:,ibin] )
			avgValErr = np.std( genTree['GPR_histVals'].array()[:,ibin] )
			avgGPhisto.SetBinContent(ibin+1,avgVal)
			avgGPhisto.SetBinError(ibin+1,avgValErr)

			avgVal = np.median( genTree['Toy_histVals'].array()[:,ibin] )
			avgValErr = np.std( genTree['Toy_histVals'].array()[:,ibin] )
			avgToyhisto.SetBinContent(ibin+1,avgVal)
			avgToyhisto.SetBinError(ibin+1,avgValErr)
		
		residGPhisto.Add( truthHisto , avgGPhisto , 1 , -1 )
		residGPhisto.Divide( truthHisto )
		
		residToyhisto.Add( truthHisto , avgToyhisto , 1 , -1 )
		residToyhisto.Divide( truthHisto )
		

		########## The remainder is all plot styling!! 
	
		### Lost of Details to Make Nice Plots!  

		canvasName = "canvas_"+inHistName
		canvas = ROOT.TCanvas(canvasName,canvasName,600,600) 
		canvas.cd() 
		upperPad = ROOT.TPad("upperPad", "upperPad", 0.0, 0.3, 1.0, 1.0)
		lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0.0, 0.0, 1.0, 0.3)
		upperPad.Draw() 
		upperPad.SetBottomMargin(0.02)
		lowerPad.Draw() 
		lowerPad.SetTopMargin(0.02) ;
		lowerPad.SetBottomMargin(0.3) ;

		# The upper pad 

		canvas.cd() 
		upperPad.cd() 
	
		yMax = 1.35*truthHisto.GetMaximum()
		leg = ROOT.TLegend(0.65,0.65,0.89,0.89)
		leg.SetBorderSize(0)
		leg.SetFillStyle(-1)

		avgToyhisto.SetLineColor(633)
		avgToyhisto.SetLineWidth(2)
		avgToyhisto.SetMarkerColor(633)
		avgToyhisto.SetMarkerSize(0)
		avgToyhisto.SetFillColorAlpha(633,0.3)
		avgToyhisto.SetFillStyle(3003)
		#avgToyhisto.SetMinimum(0)
		#avgToyhisto.SetMaximum(yMax)
		avgToyhisto.GetXaxis().SetTitleSize(0.0)
		avgToyhisto.GetXaxis().SetLabelSize(0.0)
		avgToyhisto.GetYaxis().SetTitle("Events / Bin")
		avgToyhisto.GetYaxis().SetTitleSize(0.055)
		avgToyhisto.Draw("e4 same")
		leg.AddEntry(avgToyhisto,"Avg. Toy Shape")
		
		bkgTemplateCentralToy = avgToyhisto.Clone("bkgTemplateCentralToy")
		bkgTemplateCentralToy.SetLineColor(633)
		bkgTemplateCentralToy.SetLineWidth(2)
		bkgTemplateCentralToy.SetMarkerColor(633)
		bkgTemplateCentralToy.SetMarkerSize(0)
		bkgTemplateCentralToy.SetFillStyle(0)
		bkgTemplateCentralToy.Draw("histsame")


		avgGPhisto.SetLineColor(601)
		avgGPhisto.SetLineWidth(2)
		avgGPhisto.SetMarkerColor(601)
		avgGPhisto.SetMarkerSize(0)
		avgGPhisto.SetFillColorAlpha(601,0.3)
		avgGPhisto.SetFillStyle(3003)
		#avgGPhisto.SetMinimum(0)
		#avgGPhisto.SetMaximum(yMax)
		avgGPhisto.GetXaxis().SetTitleSize(0.0)
		avgGPhisto.GetXaxis().SetLabelSize(0.0)
		avgGPhisto.GetYaxis().SetTitle("Events / Bin")
		avgGPhisto.GetYaxis().SetTitleSize(0.055)
		avgGPhisto.Draw("e4 same")
		leg.AddEntry(avgGPhisto,"Avg. GP Fit")

		bkgTemplateCentralGP = avgToyhisto.Clone("bkgTemplateCentralGP")
		bkgTemplateCentralGP.SetLineColor(601)
		bkgTemplateCentralGP.SetLineWidth(2)
		bkgTemplateCentralGP.SetMarkerColor(601)
		bkgTemplateCentralGP.SetMarkerSize(0)
		bkgTemplateCentralGP.SetFillStyle(0)
		bkgTemplateCentralGP.Draw("histsame")
		
			
		truthHisto.SetLineColor(1)
		truthHisto.SetLineWidth(1)
		truthHisto.SetMarkerSize(0)
		truthHisto.SetMinimum(0)
		truthHisto.SetMaximum(yMax)
		truthHisto.GetYaxis().SetTitle("Events / Bin")
		truthHisto.GetYaxis().SetTitleSize(0.055)
		truthHisto.GetXaxis().SetLabelSize(0.0)
		truthHisto.Draw("ehist same")
		if( not userin.useSmoothToys ): leg.AddEntry(truthHisto,"Original Template")
		else: leg.AddEntry(truthHisto,"Smooth Truth Shape")
	
		leg.Draw() 	

		ROOT.ATLAS_LABEL(0.23,0.85)
		ROOT.myText(0.34,0.85,1,"Internal")
		ROOT.myText(0.23,0.80,1,"Toy Test - Average Fit Shape",0.04)
		ROOT.myText(0.23,0.76,1,catName,0.04)
	
		canvas.Update()
		
		import pdb; pdb.set_trace() 

		# The lower pad 
		#    NOTE: If data sidebands are provided, will plot the difference of the original and 
		#    smoothed templates in reference to the data sidebands. Otherwise, will plot only the
		#    difference of the GPR smoothed template to the original. 

		canvas.cd() 
		lowerPad.cd()

		refLine = ROOT.TH1I("refLine","refLine",1,xMin,xMax)
		refLine.SetBinContent(1,0)
		refLine.SetLineColor(1)
		refLine.SetLineWidth(1)
		refLine.SetLineStyle(7)
		refLine.SetMinimum(-1.09)
		refLine.SetMaximum(1.09)
		refLine.GetXaxis().SetTitle("M_{#gamma#gamma} [GeV]")
		refLine.GetXaxis().SetTitleSize(0.13)
		refLine.GetXaxis().SetTitleOffset(1.0)
		refLine.GetXaxis().SetLabelSize(0.09)
		refLine.GetYaxis().SetTitleSize(0.11)
		refLine.GetYaxis().SetTitleOffset(0.55)
		refLine.GetYaxis().SetLabelSize(0.09)
		refLine.Draw()

					
		residGPhisto.SetLineColor(633)
		residGPhisto.SetMarkerSize(0)
		residGPhisto.Draw("ehist")
		residToyhisto.SetLineColor(601)
		residToyhisto.SetMarkerSize(0)
		residToyhisto.Draw("ehist same")
	
		refLine.GetYaxis().SetTitle("Residual [%]")
					
		refLine.Draw("same")

		canvas.Update()
		canvas.Print("ToyTest_AvgFitShape_"+catName+".eps")
		
		### Now, make a plot of the avg. fitted spurious signal for each analytic function ### 
	
		canvas2 = ROOT.TCanvas( "canvas2" , "canvas2" , 1200 , 800 ) 
		spursigGPhistos = [] 
		spursigToyhistos = [] 
		
		gprHistoGaussFits = [] 
		toyHistoGaussFits = []
		
		gprSpurSig = []
		toySpurSig = []
		
		spurSigTest_legs = [] 
		
		nSigInject = genTree['nSigEvts'].array()[0]
		
		nFuncs = len( myBkgFunctions )
		if( nFuncs <= 3 ): canvas2.Divide( nFuncs , 1 )
		elif( nFuncs <= 8 ): canvas2.Divide( int(np.ceil(nFuncs/2)) , 2 )
		else: canvas2.Divide( int(np.ceil(nFuncs/3)) , 3 )

		
		for ifunc,funcDef in enumerate(myBkgFunctions): 
			
			canvas2.GetPad( ifunc+1 ).cd() 
					
			testFunction = funcDef[0]
				
			treeName = inHistName+"_ToyTests_func_"+testFunction
			funcTree = inFile[treeName]
			
			gprSpurSig.append( funcTree['nSpurSigValue_GPR'].array() - nSigInject )
			toySpurSig.append( funcTree['nSpurSigValue_Toy'].array() - nSigInject )
			
			minSig = np.min( toySpurSig[ifunc] )
			if( np.min( gprSpurSig[ifunc] ) < minSig ): minSig = np.min( gprSpurSig[ifunc] )
			xMinsig = minSig - 0.05*(np.abs(minSig))

			maxSig = np.max( toySpurSig[ifunc] )
			if( np.max( gprSpurSig[ifunc] ) > maxSig ): maxSig = np.max( gprSpurSig[ifunc] )
			xMaxsig = maxSig + 0.05*(np.abs(maxSig))
						
			spursigGPhistos.append( ROOT.TH1D( catName+'_GPspursig' , catName+'_GPspursig' , 50 , xMinsig , xMaxsig ) )
			spursigToyhistos.append( ROOT.TH1D( catName+'_toySpurSig[ifunc]' , catName+'_toySpurSig[ifunc]' , 50 , xMinsig , xMaxsig ) )
			
			spursigGPhistos[ifunc].FillN( len(gprSpurSig[ifunc]) , gprSpurSig[ifunc] , np.ones(len(gprSpurSig[ifunc])) , 1 )
			spursigToyhistos[ifunc].FillN( len(toySpurSig[ifunc]) , toySpurSig[ifunc] , np.ones(len(toySpurSig[ifunc])) , 1 )
			
			spurSigTest_legs.append( ROOT.TLegend(0.65,0.65,0.89,0.89) )
			spurSigTest_legs[ifunc].SetBorderSize(0)
			spurSigTest_legs[ifunc].SetFillStyle(-1)
			
			spursigToyhistos[ifunc].GetXaxis().SetTitle( "Fitted Spurious Signal - Injected Signal Events" )
			if( nSigInject == 0 ): spursigToyhistos[ifunc].GetXaxis().SetTitle( "Fitted Spurious Signal" )
			spursigToyhistos[ifunc].GetYaxis().SetTitle( "Number of Entries" )
			spursigToyhistos[ifunc].SetLineColor(2)
			spursigToyhistos[ifunc].Draw("hist")
			spurSigTest_legs[ifunc].AddEntry( spursigToyhistos[ifunc] , "Raw Toy" , "l" )
			
			spursigGPhistos[ifunc].SetLineColor(4)
			spursigGPhistos[ifunc].GetXaxis().SetTitle( "Fitted Spurious Signal - Injected Signal Events" )
			if( nSigInject == 0 ): spursigGPhistos[ifunc].GetXaxis().SetTitle( "Fitted Spurious Signal" )
			spursigGPhistos[ifunc].GetYaxis().SetTitle( "Number of Entries" )
			spursigGPhistos[ifunc].Draw("hist same")
			spurSigTest_legs[ifunc].AddEntry( spursigGPhistos[ifunc] , "GP Toy" , "l" )
			
			### Fit a Gaussian to the spur sig distribution 
			
			gprFitName = "gaussGPR_fit_"+str(testFunction)
			toyFitName = "gaussToy_fit_"+str(testFunction)
		
			gprHistoGaussFits.append( ROOT.TF1(gprFitName,"[2]*TMath::Gaus(x,[0],[1])",xMinsig,xMaxsig) )
			toyHistoGaussFits.append( ROOT.TF1(toyFitName,"[2]*TMath::Gaus(x,[0],[1])",xMinsig,xMaxsig) )

			gprHistoGaussFits[ifunc].SetParLimits(0,xMinsig,xMaxsig)
			gprHistoGaussFits[ifunc].SetParLimits(0,xMinsig,xMaxsig)
			gprHistoGaussFits[ifunc].SetParameters(spursigGPhistos[ifunc].GetMean(),spursigGPhistos[ifunc].GetRMS());

			toyHistoGaussFits[ifunc].SetParLimits(1,0,xMaxsig-xMinsig)
			toyHistoGaussFits[ifunc].SetParLimits(1,0,xMaxsig-xMinsig)
			toyHistoGaussFits[ifunc].SetParameters(spursigToyhistos[ifunc].GetMean(),spursigToyhistos[ifunc].GetRMS());

			gprHistoGaussFits[ifunc].SetLineColor(601)
			toyHistoGaussFits[ifunc].SetLineColor(633)
			gprHistoGaussFits[ifunc].SetLineStyle(9)
			toyHistoGaussFits[ifunc].SetLineStyle(9)
			gprHistoGaussFits[ifunc].SetLineWidth(2)
			toyHistoGaussFits[ifunc].SetLineWidth(2)
			spursigGPhistos[ifunc].Fit(gprFitName,"emr") 
			spursigToyhistos[ifunc].Fit(toyFitName,"emr same") 
			spursigGPhistos[ifunc].DrawCopy()
			spursigToyhistos[ifunc].DrawCopy("same")	
			
			
			spurSigTest_legs[ifunc].Draw() 	

			ROOT.ATLAS_LABEL(0.23,0.85)
			ROOT.myText(0.34,0.85,1,"Internal")
			ROOT.myText(0.23,0.80,1,"Toy Test - Spurious Signal",0.04)
			ROOT.myText(0.23,0.76,1,catName,0.04)	
			ROOT.myText(0.23,0.72,1,testFunction,0.04)			
			
			
			canvas2.Update() 
		
		canvas2.Print("ToyTest_FitSigVals_"+catName+".eps")
		

	
		# Also print some useful information for the user 
		
		print(" ---------------------- ")
		print(" ---------------------- ")
		print(" Toy Test - Information ")
		print(" ---------------------- ")
		
		for ifunc,funcDef in enumerate(myBkgFunctions): 
		
			testFunction = funcDef[0]
		
			print( " --- Test Background Function: " , testFunction , " --- " )
			if( nSigInject > 0 ): 
				print("   Median Spurious Signal (Toy) - Injected Signal: " , np.median(toySpurSig[ifunc]) , " +/- " , np.std(toySpurSig[ifunc]) )
				print("   Median Spurious Signal (GP) - Injected Signal: " , np.median(gprSpurSig[ifunc]) , " +/- " , np.std(gprSpurSig[ifunc]) )
				print("      Injected Signal = " , nSigInject )			
			else: 
				print("   Median Spurious Signal (Toy): " , np.median(toySpurSig[ifunc]) , " +/- " , np.std(toySpurSig[ifunc]) )
				print("   Median Spurious Signal (GP): " , np.median(gprSpurSig[ifunc]) , " +/- " , np.std(gprSpurSig[ifunc]) )
		
			print(" -------------------- ")
		print(" -------------------- ")
				
	
	
	
if __name__== "__main__":
	main()
	
	
	